from distutils.core import setup

setup(name='PyCACalebExtras',
      version='0.9',
      description='Extra functions for PyCA',
      author='Caleb Rottman',
      author_email='crottman@sci.utah.edu',
      url='https://bitbucket.org/crottman/pycacalebextras',
      py_modules=['PyCACalebExtras'],
      )
