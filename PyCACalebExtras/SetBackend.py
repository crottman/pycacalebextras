'''allows setting the backend before anything else gets
called/imported'''
import os

# Note: when i PuTTY from laptop to topaz:
# display_env = 'localhost:10.0', but only when i have x11 forwarding


def SetBackend(backend=None):
    '''chooses the matplotlib backend. The following scenarios might happen

    matplotlib.pyplot is already imported:
       SetBackend()             # does nothing
       SetBackend('mybackend')   # tries forcibly changing backend
                                 # (unless already set to 'mybackend')

    matplotlib.pyplot isn't yet imported:
       SetBackend()             # chooses 'GTKAgg' if $DISPLAY is set
                                # chooses 'cairo' if $DISPLAY isn't set
       SetBackend('mybackend')  # chooses 'mybackend'

    It's generally best to have this call run before importing
    matplotlib, so put it at the top of your script.
    '''
    # Notes:
    # will often get this warning when opening ipython:
    # /usr/lib64/python2.7/site-packages/gtk-2.0/gtk/__init__.py:57: GtkWarning: could not open display
    #    warnings.warn(str(e), _gtk.Warning)
    # (happens ssh from topaz to atlas1) w/o -X
    # comment out: c.TerminalIPythonApp.gui = 'gtk' in profile_default/ipython_config.py

    # TESTS:
    # ssh -X from topaz to atlas1, SetBackend() (works, sets 'GTKAgg')
    # ssh from topaz to atlas1, SetBackend(),(works, sets 'cairo')
    # putty to topaz (?)
    # putty to atlas1 (?)

    # if backend != 'cairo':
    #     try:                    # try to connect to X display
    #         from Xlib import display
    #         display.Display()
    #     except ConnectionClosedError:
    #         backend = 'cairo'

    # note: this else statment doesn't work like i would like, but is
    # hopefully not necessary: just don't enable X11 forwarding in putty

    # else:                       # try connecting to display
    #     import socket
    #     # parse the display string
    #     display_host, display_num = display_env.split(':')
    #     # display_num_major, display_num_minor = display_num.split('.')
    #     if '.' in display_num:
    #         display_num_major = display_num.split('.')[0]
    #     else:
    #         display_num_major = display_host

    #     # calculate the port number
    #     display_port = 6000 + int(display_num_major)

    #     # attempt a TCP connection
    #     sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #     try:
    #         sock.connect((display_host, display_port))
    #     except socket.error:
    #         backend = 'cairo'
    #     finally:
    #         sock.close()

    import matplotlib
    try:
        matplotlib.pyplot       # test if pyplot has already been called
        backend_set = True
    except AttributeError:      # throws this if it hasn't
        backend_set = False

    # if backend is already set and we aren't trying to force a change, return
    if backend_set:
        import matplotlib.pyplot as plt
        plt.ion()
        if backend is None or backend.lower() == matplotlib.get_backend().lower():
            pass                # don't change
        else:                   # try to force change
            plt.switch_backend(backend)  # this is *experimental*
        return plt

    if backend is None:         # try to find appropriate backend
        try:
            display_env = os.environ['DISPLAY']
        except KeyError:
            display_env = ''

        if display_env == '':   # can't connect to display
            backend = 'cairo'
        else:
            backend = 'GTkAgg'

    # backend is now a string
    matplotlib.use(backend)
    import matplotlib.pyplot as plt
    plt.ion()
    return plt


def GetAvailableBackends():
    '''returs a list of matplot lib backends (doesn't not work yet)'''
    print 'Not Yet Working...'


def PrintAvailableBackends():
    '''prints backend info (from stackoverflow answer)'''

    from pylab import ion, clf, plot, draw, ioff
    from numpy import arange, pi, sin
    import time

    import matplotlib.backends
    import matplotlib.pyplot as p
    import os.path

    def is_backend_module(fname):
        """Identifies if a filename is a matplotlib backend module"""
        return fname.startswith('backend_') and fname.endswith('.py')

    def backend_fname_formatter(fname):
        """Removes the extension of the given filename, then takes away the leading 'backend_'."""
        return os.path.splitext(fname)[0][8:]

    # get the directory where the backends live
    backends_dir = os.path.dirname(matplotlib.backends.__file__)

    # filter all files in that directory to identify all files which provide a backend
    backend_fnames = filter(is_backend_module, os.listdir(backends_dir))

    backends = [backend_fname_formatter(fname) for fname in backend_fnames]

    print "supported backends: \t" + str(backends)

    # validate backends
    backends_valid = []
    for b in backends:
        try:
            p.switch_backend(b)
            backends_valid += [b]
        except:
            continue

    print "valid backends: \t" + str(backends_valid)

    # try backends performance
    for b in backends_valid:
        ion()
        try:
            p.switch_backend(b)
            clf()
            tstart = time.time()               # for profiling
            x = arange(0, 2*pi, 0.01)            # x-array
            line, = plot(x, sin(x))
            for i in arange(1, 200):
                line.set_ydata(sin(x+i/10.0))  # update the data
                draw()                         # redraw the canvas

            print b + ' FPS: \t', 200/(time.time()-tstart)
            ioff()

        except:
            print b + " error :("


def TryInteractiveBackends():
    '''prints backend info (from stackoverflow answer)'''

    from pylab import ion, clf, plot, draw, ioff
    from numpy import arange, pi, sin
    import time

    import matplotlib.backends
    import matplotlib.pyplot as p
    import os.path

    def is_backend_module(fname):
        """Identifies if a filename is a matplotlib backend module"""
        return fname.startswith('backend_') and fname.endswith('.py')

    def backend_fname_formatter(fname):
        """Removes the extension of the given filename, then takes away the leading 'backend_'."""
        return os.path.splitext(fname)[0][8:]

    # get the directory where the backends live
    backends_dir = os.path.dirname(matplotlib.backends.__file__)

    # filter all files in that directory to identify all files which provide a backend
    backend_fnames = filter(is_backend_module, os.listdir(backends_dir))

    backends = [backend_fname_formatter(fname) for fname in backend_fnames]

    print "supported backends: \t" + str(backends)

    # validate backends
    backends_valid = []
    for b in backends:
        try:
            p.switch_backend(b)
            backends_valid += [b]
        except:
            continue

    print "valid backends: \t" + str(backends_valid)

    ibackends = ['tkagg', 'gtk', 'gtkagg']

    backends_interactive = list(set(backends_valid) &
                                set(ibackends))

    print "interactive backends (probably):", backends_interactive

    # try backends performance
    backends_show = []
    for b in backends_interactive:

        p.switch_backend(b)
        p.close('all')

        TestMultipleImages()

        p.close('all')
        ioff()
    print "showing backends: \t" + str(backends_show)


def TestMultipleImages():
    import numpy as np
    import matplotlib.pyplot as plt
    import time
    print "Using backend:", plt.get_backend()

    img = plt.figure()

    # ion is necessary if you ever call plt.show()
    # without, you need to close figure before code will continue
    # plt.ion()
    # plt.ioff()

    plt.close('all')
    for i in xrange(15):
        try:
            print i
            plt.clf()
            arr = np.random.randn(20, 20)
            img = plt.imshow(arr)
            # plt.draw()          # not necessary
            plt.show(block=False)
            # plt.show()          # not necessary (if ion)
            # time.sleep(.2)
            plt.pause(0.000001)  # necessary
            # plt.waitforbuttonpress(timeout=0.00001)
        except TypeError:
            continue
    plt.close('all')

def main():
    TryInteractiveBackends()

if __name__ == "__main__":
    main()
    # import matplotlib
    # import time
    # matplotlib.use('gtkagg')
    # import matplotlib.pyplot as plt
    # plt.ion()
    # plt.figure()
    # # plt.show()
    # # plt.draw()
    # plt.pause(0.0001)
    # time.sleep(1)
