'''Commonly used functions for PyCA - Caleb Rottman'''
import os
import errno
import math
import numpy as np
import time
import subprocess

import PyCA.Core as ca
import PyCA.Common as common


def IsNumber(a):
    '''determines if the input is a single number'''
    try:
        float(a)
    except (TypeError, ValueError):
        return False
    return True


def SetGaussRandom(im, mu=1, sigma=1):
    '''Sets image with mean mu and std sigma to gaussian random noise'''
    oldType = im.memType()
    im.toType(ca.MEM_HOST)
    im.asnp()[:, :, :] = sigma * np.random.randn(im.size().x,
                                                 im.size().y, im.size().z) + mu
    im.toType(oldType)


def AddGaussianNoise(im, sigma=1, mu=0):
    '''Adds gaussian noise to image'''
    oldType = im.memType()
    im.toType(ca.MEM_HOST)
    im.asnp()[:, :, :] = im.asnp()[:, :, :] + \
        sigma * np.random.randn(im.size().x,
                                im.size().y, im.size().z) + mu
    im.toType(oldType)


def Laplacian(lap, Im, scratchI=None):
    '''Takes the central differences laplacian of an image'''
    if scratchI is None:
        l1 = ca.Image3D(Im.grid(), Im.memType())
    else:
        l1 = scratchI
    if id(lap) == id(Im):
        raise Exception('PyCACalebExtras:Common.py:Laplacian:' +
                        ' lap and Im must be unique')

    v1 = ca.Vec3Df(1.0, 0.0, 0.0)
    v2 = ca.Vec3Df(0.0, 1.0, 0.0)
    v3 = ca.Vec3Df(-1.0, 0.0, 0.0)
    v4 = ca.Vec3Df(0.0, -1.0, 0.0)

    ca.ComposeTranslation(l1, Im, v1)
    ca.Copy(lap, l1)
    ca.ComposeTranslation(l1, Im, v2)
    ca.Add_I(lap, l1)
    ca.ComposeTranslation(l1, Im, v3)
    ca.Add_I(lap, l1)
    ca.ComposeTranslation(l1, Im, v4)
    ca.Add_I(lap, l1)
    if Im.size().z > 1:
        v5 = ca.Vec3Df(0.0, 0.0, 1.0)
        v6 = ca.Vec3Df(0.0, 0.0, 1.0)
        ca.ComposeTranslation(l1, Im, v5)
        ca.Add_I(lap, l1)
        ca.ComposeTranslation(l1, Im, v6)
        ca.Add_I(lap, l1)
        ca.Add_MulC_I(lap, Im, -6.0)
    else:
        ca.Add_MulC_I(lap, Im, -4.0)


def Laplacian2(lap, Im):
    '''Takes the central differences laplacian of an
    image using convolution with a kernel

    so far this is slower than the previous method
    '''
    if id(lap) == id(Im):
        raise Exception('PyCACalebExtras:Common.py:Laplacian:' +
                        ' lap and Im must be unique')
    if Im.size().z > 1:
        grid = ca.GridInfo(ca.Vec3Di(3, 3, 3))
        klap = ca.Image3D(grid, ca.MEM_HOST)
        ca.SetMem(klap, 0.0)
        klap.asnp()[1, 0, 1] = 1.0
        klap.asnp()[0, 1, 1] = 1.0
        klap.asnp()[1, 2, 1] = 1.0
        klap.asnp()[2, 1, 1] = 1.0
        klap.asnp()[1, 1, 0] = 1.0
        klap.asnp()[1, 1, 2] = 1.0
        klap.asnp()[1, 1, 1] = -6.0
    else:
        grid = ca.GridInfo(ca.Vec3Di(3, 3, 1))
        klap = ca.Image3D(grid, ca.MEM_HOST)
        ca.SetMem(klap, 0.0)
        klap.asnp()[1, 0, :] = 1.0
        klap.asnp()[0, 1, :] = 1.0
        klap.asnp()[1, 2, :] = 1.0
        klap.asnp()[2, 1, :] = 1.0
        klap.asnp()[1, 1, :] = -4.0

    klap.toType(lap.memType())
    ca.Convolve(lap, Im, klap)


def ConvolveSeparable(Imfilt, Im, kernels, normalize=True):
    '''does a convolution with (up to) 3 separable kernels:
    '''
    assert id(Imfilt) != id(Im)

    tmp = Im.copy()

    kx = common.ImFromNPArr(np.array(kernels[0]), Im.memType())
    ky = common.ImFromNPArr(np.array(kernels[1]), Im.memType())
    try:
        kz = common.ImFromNPArr(np.array(kernels[2]), Im.memType())
    except ValueError:
        kz = None
    if normalize:
        kx /= ca.Sum(kx)
        ky /= ca.Sum(ky)
        if kz is not None:
            kz /= ca.Sum(kz)

    ca.Convolve(Imfilt, Im, kx)
    ca.Convolve(tmp, Imfilt, ky)
    if kz is not None:
        ca.Convolve(Imfilt, tmp, kz)
    else:
        ca.Copy(Imfilt, tmp)


def RectFilter(Imfilt, Im, size):
    '''implements a separable box filter for an image'''

    if IsNumber(size):
        if Is3D(Im):
            size = [size, size, size]
        else:
            size = [size, size, 1]

    kernels = [[1]*sz for sz in size]
    ConvolveSeparable(Imfilt, Im, kernels)


def GaussianKernel2D(radius, sigma, memType=ca.MEM_DEVICE):
    '''returns a 2D Gaussian kernel'''
    r = radius
    d = 2*int(radius) + 1
    grid = ca.GridInfo(ca.Vec3Di(d, d, 1))
    kgauss = ca.Image3D(grid, ca.MEM_HOST)

    xx, yy = np.meshgrid(range(-r, d-r), range(-r, d-r))
    npgauss = np.exp(-1.0/(2.0*sigma) * (xx**2 + yy**2))

    kgauss.asnp()[:, :, :] = npgauss.reshape((d, d, 1))
    kgauss.toType(memType)
    kgauss /= ca.Sum(kgauss)

    return kgauss


def BoxKernel2D(radius, memType=ca.MEM_DEVICE):
    '''returns a 2D box kernel'''
    d = 2*int(radius) + 1
    kgrid = ca.GridInfo(ca.Vec3Di(d, d, 1),       # size
                        ca.Vec3Df(1.0, 1.0, 1.0),  # spacing
                        ca.Vec3Df(0.0, 0.0, 0.0))  # origin
    kernel = ca.Image3D(kgrid, memType)
    ca.SetMem(kernel, 1.0/kernel.nVox())
    return kernel


def NormalizeImList(gN):
    '''normalizes ImList to 1

    if ImList = [im1, im2, ..., imn], then in the new list
    sqrt(im1^2 + ... + imn^2) = 1 for all x

    '''
    grid = gN[0].grid()
    mType = gN[0].memType()
    N = len(gN)
    sN = [ca.Image3D(grid, mType) for _ in range(N)]
    normg = ca.Image3D(grid, mType)
    scratchI = ca.Image3D(grid, mType)

    for k in range(N):
        ca.Sqr(sN[k], gN[k])    # square of gN
    AddImList(scratchI, sN)           # sum of sN

    ca.Sqrt(normg, scratchI)    # norm
    for k in range(N):
        ca.Div_I(gN[k], normg)


def AddImList(newSum, imList):
    ''' Adds an entire list of images, where newSum is
    and *3D and imList is a list of *3Ds'''
    ca.SetMem(newSum, 0.0)
    for k in xrange(len(imList)):
        ca.Add_I(newSum, imList[k])


def DotProductImList(dotp, aN, bN):
    ''' inner products two lists: dotp = <aN, bN>
    aN, bN are lists of Image3Ds, dotp is a Image3D '''
    ca.SetMem(dotp, 0.0)
    for k in xrange(len(aN)):
        ca.Add_Mul_I(dotp, aN[k], bN[k])


def SetRange_I(im, rng=None):
    '''Sets the intensity range of an image
    defaults to [0, 1]

    note: also takes arguments for rng:
    'char','uchar', 'short', 'ushort'
    '''
    if rng is None:
        rng = [0.0, 1.0]
    elif rng == 'char':
        rng = [-128, 127]
    elif rng == 'uchar':
        rng = [0, 255]
    elif rng == 'short':
        rng = [-32768, 32767]
    elif rng == 'ushort':
        rng = [0, 65525]
    assert rng[1] > rng[0]

    if isinstance(im, (ca.Field3D, ca.Image3D)):
        [im_min, im_max] = ca.MinMax(im)
        if im_min == im_max:
            return
        ca.SubC_I(im, im_min)
        ca.MulC_I(im, 1.0*(rng[1] - rng[0]) / (im_max - im_min))
        ca.AddC_I(im, rng[0])
    else:                       # is a list of *3Ds
        [im_min, im_max] = MinMaxList(im)
        if im_min == im_max:
            return
        for im_i in im:
            ca.SubC_I(im_i, im_min)
            ca.MulC_I(im_i, 1.0*(rng[1] - rng[0]) / (im_max - im_min))
            ca.AddC_I(im_i, rng[0])


def SetRange(imout, im, rng):
    '''Sets the intensity range of an image'''
    ca.Copy(imout, im)
    # import PyCACalebExtras.Display as cd

    # cd.DispImage(imout)
    SetRange_I(imout, rng)


def NPArrToIm(arr, im):
    '''Converts an Image3D to a numpy Array'''
    oldType = im.memType()
    im.toType(ca.MEM_HOST)
    im.asnp()[:, :, :] = arr
    im.toType(oldType)


def BinaryInverse(iminv, im):
    '''iminv = !im (where im is an image of numbers

    iminv = -im+1

    '''
    ca.Neg(iminv, im)
    ca.AddC_I(iminv, 1.0)


def BinaryInverse_I(im):
    '''im = !im (where im is an image of numbers

    im = -im+1

    '''
    BinaryInverse(im, im)


def IsBinary(im):
    '''returns true if an image is only ones and zeros'''
    imc = im.copy()
    imc -= 0.5
    ca.Abs_I(imc)
    mm = ca.MinMax(imc)
    return mm[0] == mm[1] == 0.5


def MakeBinary_I(im, threshold=0.5):
    '''makes an image binary:
    anything <= threshold goes to 0
    anything > threshold goes to 1'''
    im -= threshold
    ca.Ramp_I(im)
    from .Logic import SetRegionGT
    SetRegionGT(im, im, 0.0, 1.0)


def SubVol(ob, xrng=None, yrng=None, zrng=None):
    '''returns a subvolume image/field given the
    x, y, and z ranges.

    If you give xrng = [10, None], it will go to the
    end of the image'''

    if xrng is None:
        xrng = [0, ob.size().x]
    if yrng is None:
        yrng = [0, ob.size().y]
    if zrng is None:
        zrng = [0, ob.size().z]
    if xrng[1] is None:
        xrng[1] = ob.size().x
    if yrng[1] is None:
        yrng[1] = ob.size().y
    if zrng[1] is None:
        zrng[1] = ob.size().z

    sizeout = [xrng[1]-xrng[0], yrng[1]-yrng[0], zrng[1]-zrng[0]]
    originout = [ob.origin().x + ob.spacing().x*xrng[0],
                 ob.origin().y + ob.spacing().y*yrng[0],
                 ob.origin().z + ob.spacing().z*zrng[0]]
    gridout = MakeGrid(sizeout, ob.spacing(), originout)

    if isinstance(ob, ca.Image3D):
        obout = ca.Image3D(gridout, ob.memType())
    else:
        obout = ca.Field3D(gridout, ob.memType())

    ca.SubVol(obout, ob, ca.Vec3Di(xrng[0], yrng[0], zrng[0]))

    return obout


def BisectImage(im, xrng, yrng, zrng=None):
    '''returns an Image3D/Field3D that is a rectangular subsection of im
    '''

    print "Warning, depreciated: Use PyCACalebExtras.SubVol"

    import PyCA.Common as common

    if xrng is None:
        xrng = [0, im.size().x]
    if yrng is None:
        yrng = [0, im.size().y]
    if zrng is None:
        zrng = [0, im.size().z]

    oldType = im.memType()
    im.toType(ca.MEM_HOST)
    if isinstance(im, ca.Image3D):
        if Is2D(im):
            imBisectArr = np.squeeze(im.asnp())[yrng[0]:yrng[1], xrng[0]:xrng[1]]
        else:
            imBisectArr = im.asnp()[xrng[0]:xrng[1], yrng[0]:yrng[1], zrng[0]:zrng[1]]
        imBisect = common.ImFromNPArr(imBisectArr, oldType)
    else:                       # Field Bisecting!
        if Is2D(im):
            shape = [yrng[1]-yrng[0], xrng[1]-xrng[0]]
            imBisectArr = np.zeros((shape[0], shape[1], 3))
            imBisectArr[:,:,0] = np.squeeze(im.asnp()[0][yrng[0]:yrng[1], xrng[0]:xrng[1], :])
            imBisectArr[:,:,1] = np.squeeze(im.asnp()[1][yrng[0]:yrng[1], xrng[0]:xrng[1], :])
            imBisectArr[:,:,2] = np.squeeze(im.asnp()[2][yrng[0]:yrng[1], xrng[0]:xrng[1], :])
        else:
            # NOT TESTED!!!
            shape = [xrng[1]-xrng[0], yrng[1]-yrng[0], zrng[1]-zrng[0]]
            imBisectArr = np.zeros((shape[0], shape[1], shape[2], 3))
            imBisectArr[:,:,:,0] = im.asnp()[0][xrng[0]:xrng[1], yrng[0]:yrng[1], zrng[0]:zrng[1]]
            imBisectArr[:,:,:,1] = im.asnp()[1][xrng[0]:xrng[1], yrng[0]:yrng[1], zrng[0]:zrng[1]]
            imBisectArr[:,:,:,2] = im.asnp()[2][xrng[0]:xrng[1], yrng[0]:yrng[1], zrng[0]:zrng[1]]
        imBisect = common.FieldFromNPArr(imBisectArr, oldType)

    imBisect.setSpacing(im.spacing())
    # increase origin, if necessary
    imBisect.setOrigin(ca.Vec3Df(im.origin().x + xrng[0]*im.spacing().x,
                                 im.origin().y + yrng[0]*im.spacing().y,
                                 im.origin().z + zrng[0]*im.spacing().z))

    im.toType(oldType)
    return imBisect


def FlipDim(Im, axis):
    '''Flip dimension 0, 1, or 2 of an Image3D/Field3D'''
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    if isinstance(Im, ca.Image3D):
        Imout = ca.Image3D(Im.grid(), Im.memType())
        if axis == 0:
            Imout.asnp()[:, :, :] = Im.asnp()[::-1, :, :]
        elif axis == 1:
            Imout.asnp()[:, :, :] = Im.asnp()[:, ::-1, :]
        elif axis == 2:
            Imout.asnp()[:, :, :] = Im.asnp()[:, :, ::-1]
        else:
            raise Exception("axis must be 0, 1, or 2")
    elif isinstance(Im, ca.Field3D):
        Imout = ca.Field3D(Im.grid(), Im.memType())
        if axis == 0:
            Imout.asnp()[0][:, :, :] = Im.asnp()[0][::-1, :, :]
            Imout.asnp()[1][:, :, :] = Im.asnp()[1][::-1, :, :]
            Imout.asnp()[2][:, :, :] = Im.asnp()[2][::-1, :, :]
        elif axis == 1:
            Imout.asnp()[0][:, :, :] = Im.asnp()[0][:, ::-1, :]
            Imout.asnp()[1][:, :, :] = Im.asnp()[1][:, ::-1, :]
            Imout.asnp()[2][:, :, :] = Im.asnp()[2][:, ::-1, :]
        elif axis == 2:
            Imout.asnp()[0][:, :, :] = Im.asnp()[0][:, :, ::-1]
            Imout.asnp()[1][:, :, :] = Im.asnp()[1][:, :, ::-1]
            Imout.asnp()[2][:, :, :] = Im.asnp()[2][:, :, ::-1]
        else:
            raise Exception("axis must be 0, 1, or 2")
    Imout.toType(oldType)
    Im.toType(oldType)
    return Imout


def ConcatImages(imList, axis=0):
    '''Concatonates a list of Image3Ds along the axis

    axis = 0 refers to the x-axis (as viewed in DispImage)
    axis = 1 refers to the y-axis (as viewed in DispImage)

    '''
    import PyCA.Common as common

    if any(isinstance(im, ca.Field3D) for im in imList):
        flist = [ImtoField(im) for im in imList]
        return ConcatFields(flist, axis)

    # switch axis to visual x/y axis
    if axis == 1:
        axis = 0
    elif axis == 0:
        axis = 1

    oldType = []
    imarrlist = []
    for image in imList:
        oldType.append(image.memType())
        image.toType(ca.MEM_HOST)
        imarrlist.append(image.asnp())
    imCat = common.ImFromNPArr(np.concatenate(imarrlist, axis), oldType[0])
    for i in xrange(len(imList)):
        imList[i].toType(oldType[i])
    return imCat


def ConcatFields(vfList, axis=0):
    '''Concatonates a list of Image3Ds along the axis

    axis = 0 refers to the x-axis (as viewed in DispImage)
    axis = 1 refers to the y-axis (as viewed in DispImage)

    '''
    import PyCA.Common as common

    # switch axis to visual x/y axis
    if axis == 1:
        axis = 0
    elif axis == 0:
        axis = 1

    oldType = []
    vfarrlist = []
    for field in vfList:
        oldType.append(field.memType())
        field.toType(ca.MEM_HOST)
        vfarrlist.append(np.concatenate(field.asnp(), 2))
    imCat = common.FieldFromNPArr(np.concatenate(vfarrlist, axis), oldType[0])
    for i, vf in enumerate(vfList):
        vf.toType(oldType[i])
    return imCat


def ClampImageMin(Imclamped, Im, clampVal):
    '''Clamps an image by a lower bound

    given Im, returns Imclamped such that
    Imclamped(Im < clampVal) = clampVal

    '''
    if id(Imclamped) != id(Im):
        ca.Copy(Imclamped, Im)
    ca.SubC_I(Imclamped, clampVal)
    ca.Ramp_I(Imclamped)
    ca.AddC_I(Imclamped, clampVal)


def ClampImageMin_I(Im, clampVal):
    '''Clamps an image by a lower bound

    given Im, returns Im such that
    Im(Im < clampVal) = clampVal

    '''
    ClampImageMin(Im, Im, clampVal)
    # ca.SubC_I(Im, clampVal)
    # ca.Ramp_I(Im)
    # ca.AddC_I(Im, clampVal)


def ClampImageMax(Imclamped, Im, clampVal):
    '''Clamps an image by an upper bound

    given Im, returns Imclamped such that
    Imclamped(Im > clampVal) = clampVal

    '''
    if id(Imclamped) != id(Im):
        ca.Copy(Imclamped, Im)
    ca.Neg_I(Imclamped)
    ClampImageMin_I(Imclamped, -clampVal)
    ca.Neg_I(Imclamped)
    if ca.Min(Imclamped) == 0.0:
        ca.Ramp_I(Imclamped)      # avoid -0.0


def ClampImageMax_I(Im, clampVal):
    '''Clamps an image by a upper bound

    given Im, returns Im such that
    Im(Im > clampVal) = clampVal

    '''
    ClampImageMax(Im, Im, clampVal)


def SafeDiv(Iout, num, denom, eps=1e-7, scratchI=None):
    '''Divides an image safely to avoid Inf or Nan values

    Iout = num/Max(denom, eps) for positive images
    Iout = num/Min(denom, -eps) for negative images
    '''
    if scratchI is None:
        scratchI = ca.Image3D(Iout.grid(), Iout.memType())

    mm = ca.MinMax(denom)
    if mm[1] > 0:               # if image is positive in general
        ClampImageMin(scratchI, denom, abs(eps))
    else:
        ClampImageMax(scratchI, denom, -abs(eps))
    ca.Div(Iout, num, scratchI)

    mm = ca.MinMax(Iout)
    if math.isinf(mm[0]) or math.isinf(mm[1]):
        print "Still getting inf values in SafeDiv: use a larger epsilon"


def SafeDiv_I(Im, denom, eps=1e-7, scratchI=None):
    '''Divides an image safely to avoid Inf or Nan values

    Im /= Max(denom, eps) for positive images
    Im /= Min(denom, -eps) for negative images
    '''
    SafeDiv(Im, Im, denom, eps, scratchI)


def ConvolveField2(fConvolved, V, kernel, scratchI1=None, scratchI2=None):
    '''Convolves a Field3D component-wise (for the x and y component)
    kernel is either an Image3D or a PyCA gaussian filter
    fConvolved(i) = V(i) (x) kernel
    '''
    scratchI1 = ca.Image3D(V.grid(), V.memType())
    scratchI2 = ca.Image3D(V.grid(), V.memType())
    for i in xrange(2):
        ca.Copy(scratchI1, V, i)
        if isinstance(kernel, ca.Image3D):
            ca.Convolve(scratchI2, scratchI1, kernel)

        elif (isinstance(kernel, ca.GaussianFilterGPU) or
              isinstance(kernel, ca.GaussianFilterCPU)):
            scratchI3 = ca.Image3D(V.grid(), V.memType())
            kernel.filter(scratchI2, scratchI1, scratchI3)
        else:
            raise Exception("kernel must be Image3D or GaussianFilter*PU")
        ca.Copy(fConvolved, scratchI2, i)


def ConvolveField2_I(V, kernel, scratchI1=None, scratchI2=None):
    '''Convolves a Field3D component-wise (for the x and y component)
    kernel is either an Image3D or a PyCA gaussian filter
    V(i) = V(i) (x) kernel
    '''
    ConvolveField2(V, V, kernel, scratchI1, scratchI2)


def ConvolveField(fConvolved, V, kernel, scratchI1=None, scratchI2=None):
    '''Convolves a Field3D component-wise by an Image3D kernel
    fConvolved(i) = V(i) (x) kernel
    '''
    if scratchI1 is None:
        scratchI1 = ca.Image3D(V.grid(), V.memType())
    if scratchI2 is None:
        scratchI2 = ca.Image3D(V.grid(), V.memType())
    for i in xrange(3):
        ca.Copy(scratchI1, V, i)
        ca.Convolve(scratchI2, scratchI1, kernel)
        ca.Copy(fConvolved, scratchI2, i)


def ConvolveField_I(V, kernel, scratchI1=None, scratchI2=None):
    '''Convolves a Field3D component-wise by an Image3D kernel
    fConvolved(i) = V(i) (x) kernel
    '''
    ConvolveField(V, V, kernel, scratchI1, scratchI2)


def JacDetV(jacdet, v):
    '''Jacobian Determinant of a vector field v'''
    ca.VtoH_I(v)
    ca.JacDetH(jacdet, v)
    ca.HtoV_I(v)


def JacDetH(jacdet, h):
    '''Jacobian Determinant of a H-field h'''
    ca.JacDetH(jacdet, h)


def Is2D(im):
    '''returns True if the Image3D or Field3D is 2D'''
    return im.size().x == 1 or im.size().y == 1 or im.size().z == 1


def Is3D(im):
    '''returns True if the Image3D or Field3D is 2D'''
    return not Is2D(im)


def SelectMemory():
    '''mType = SelectMemory returns ca.MEM_DEVICE if available,
    otherwise returns ca.MEM_HOST'''
    if ca.GetNumberOfCUDADevices() > 0:
        mType = ca.MEM_DEVICE
    else:
        print "No CUDA devices found, running on CPU"
        mType = ca.MEM_HOST
    return mType


def Im_to_Imlist(Im):
    '''Takes a single 3D Image3D, and returns a list of 2D Image3Ds,
    split perpindicular to the 'z' axis'''
    import PyCA.Common as common

    Imlist = []
    if isinstance(Im, ca.Image3D):
        for i in range(Im.size().z):
            Imlist.append(common.ExtractSliceIm(Im, i, dim='z'))
    else:
        for i in range(Im.size().z):
            Imlist.append(common.ExtractSliceVF(Im, i, dim='z'))

    return Imlist


def Imlist_to_Im(Imlist):
    '''Takes a list of (same sized 2D) Image3Ds/Field3Ds and returns a
    single 3D Image3D/Field3D that has the list concatanated along the
    z axis'''
    grid = ca.GridInfo(ca.Vec3Di(Imlist[0].size().x,
                                 Imlist[0].size().y,
                                 len(Imlist)),
                       Imlist[0].grid().spacing(),
                       Imlist[0].grid().origin())
    # allow color images/ fields
    if isinstance(Imlist[0], ca.Image3D):
        Im = ca.Image3D(grid, Imlist[0].memType())
    elif isinstance(Imlist[0], ca.Field3D):
        Im = ca.Field3D(grid, Imlist[0].memType())

    for i in xrange(len(Imlist)):
        vec = ca.Vec3Di(0, 0, i)

        # print 'Start: ', vec
        # print '2D size; ', Imlist[i].grid().size()
        # print '3D size; ', Im.grid().size()
        # print '-----------------------'

        ca.SetSubVol_I(Im, Imlist[i], vec)
    return Im


def PadImage(Im, new_size, PadVal=0.0, surround=False):
    '''returns a padded Image3D/Field3D

    Im: input PyCA Image3D/Field3D
    new_size: list of the new size (e.g. [64, 64, 64])
    PadVal: value padded on the image
    surround: if true, center image, if false, original image is at 0,0,0
    '''
    # print type(new_size)
    if isinstance(new_size, ca.Vec3Di):
        grid = ca.GridInfo(new_size)
        new_size = [new_size.x, new_size.y, new_size.z]
    else:
        if len(new_size) is 2:
            new_size.append(1)
        grid = ca.GridInfo(ca.Vec3Di(new_size[0], new_size[1], new_size[2]))
    if isinstance(Im, ca.Field3D):
        Imout = ca.Field3D(grid, Im.memType())
    else:
        Imout = ca.Image3D(grid, Im.memType())
    # keep same spacing and origin as previous
    # Imout.setSpacing(Im.grid().spacing())
    # Imout.setOrigin(Im.grid().origin())
    Imout.setGrid(ca.GridInfo(grid.size(), Im.grid().spacing(), Im.grid().origin()))
    ca.SetMem(Imout, PadVal)
    old_size = [Im.size().x, Im.size().y, Im.size().z]
    if any((old_size[0] > new_size[0],
            old_size[1] > new_size[1],
            old_size[2] > new_size[2])):
        print 'old size: ', old_size
        print 'new size: ', new_size
        raise Exception('PyCACalebExtras.Common: PadImage:' +
                        'The new size must be larger than the old size')
    if surround:
        start = ca.Vec3Di(int((new_size[0]-old_size[0])/2),
                          int((new_size[1]-old_size[1])/2),
                          int((new_size[2]-old_size[2])/2))
    else:
        start = ca.Vec3Di(0, 0, 0)
    ca.SetSubVol_I(Imout, Im, start)
    return Imout


def ResampleToSize(Im, new_size):
    '''Resamples an image to a newsized image
    '''
    import PyCA.Common as common

    if isinstance(new_size, ca.Vec3Di):
        new_size = [new_size.x, new_size.y, new_size.z]
    else:
        if len(new_size) is 2:
            new_size.append(1)
    Iout = ca.Image3D(Im.grid(), Im.memType())
    old_size = [Im.size().x, Im.size().y, Im.size().z]
    ratios = [(1.0*nsz)/osz for osz, nsz in zip(old_size, new_size)]
    ratio = min(ratios)
    origin = [0, 0, 0]
    # new image should be centered
    t0 = (new_size[0]-ratio*old_size[0])/2
    t1 = (new_size[1]-ratio*old_size[1])/2
    t2 = (new_size[2]-ratio*old_size[2])/2
    if Is3D(Im):
        A = np.matrix([[ratio, 0, 0, t0],
                       [0, ratio, 0, t1],
                       [0, 0, ratio, t2],
                       [0, 0, 0, 1]])
    elif Is2D(Im):
        A = np.matrix([[ratio, 0, t0],
                       [0, ratio, t1],
                       [0, 0, 1]])
    from . import ImageProcessing as ip
    ip.ApplyAffine(Iout, Im, A, origin, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
    # Iout = common.ExtractROI(Iout, [0, new_size[0], 0, new_size[1], 0, new_size[2]])
    Iout = SubVol(Iout, [0, new_size[0]], [0, new_size[1]], [0, new_size[2]])
    return Iout


def CenterImage(Im, justReturnT=False):
    '''Centers an image by putting its center of mass in the middle of
    the image'''
    origin = [(Im.grid().size().x+1)/2.0, (Im.grid().size().y+1)/2.0]
    if Is2D(Im):
        h = ca.Field3D(Im.grid(), Im.memType())
        Itrans = ca.Image3D(Im.grid(), Im.memType())
        ca.SetToIdentity(h)
        x = ca.Image3D(Im.grid(), Im.memType())
        y = ca.Image3D(Im.grid(), Im.memType())
        ca.Copy(x, h, 0)
        ca.Copy(y, h, 1)
        x -= origin[0]
        y -= origin[1]
        x *= Im
        y *= Im

        if ca.Sum(Im) > 0:
            t_x = ca.Sum(x)/ca.Sum(Im)
            t_y = ca.Sum(y)/ca.Sum(Im)
        else:
            t_x, t_y = 0, 0
        t = ca.Vec3Df(int(t_x), int(t_y), 0.0)

        if not justReturnT:
            ca.ComposeTranslation(Itrans, Im, t, ca.BACKGROUND_STRATEGY_PARTIAL_ZERO)
            ca.Copy(Im, Itrans)
        return (-int(t_x), -int(t_y))


def MinMaxList(Imlist):
    '''returns [imlistmin, imlistmax] which contains the min and max
    values in a list of Image3Ds'''
    # nIm = len(Imlist)
    imRangeList = [ca.MinMax(im) for im in Imlist]
    imRangeList = zip(*imRangeList)
    minMax = [min(imRangeList[0]), max(imRangeList[1])]
    return minMax


def FilterField(g, F_filt, F):
    '''filter a Field3D'''
    tmp = ca.Image3D(F.grid(), F.memType())
    Im = ca.Image3D(F.grid(), F.memType())
    Im_filt = ca.Image3D(F.grid(), F.memType())
    for i in xrange(3):
        ca.Copy(Im, F, i)
        g.filter(Im_filt, Im, tmp)
        ca.Copy(F_filt, Im_filt, i)


def SetVoxel(Im, Index, val):
    '''abstract version of Image3D.set'''
    if not isinstance(Index, ca.Vec3Di):
        Index = ca.Vec3Di(int(Index[0]),
                          int(Index[1]),
                          int(Index[2]))
    if Im.memType() == ca.MEM_DEVICE:
        voxel = ca.Image3D(ca.GridInfo(ca.Vec3Di(1, 1, 1),
                                       Im.spacing(),
                                       Im.origin()),
                           Im.memType())
        ca.SetMem(voxel, val)
        ca.SetSubVol_I(Im, voxel, Index)
    else:
        Im.set(Index.x, Index.y, Index.z, val)


def DownsampleBin(Im, ds):
    '''Downsamples an Image, sets the new grid (spacing) accordingly'''
    import PyCA.Common as common

    oldtype = Im.memType()
    Im.toType(ca.MEM_HOST)
    if isinstance(Im, ca.Image3D):
        if Is2D(Im):
            Imnp = Im.asnp()
            # trim Imnp so that its axes are divisible by ds
            Imnptrim = Imnp[0:Imnp.shape[0]/ds*ds, 0:Imnp.shape[1]/ds*ds]
            ImSm = sum([Imnptrim[i::ds, j::ds]
                        for i in range(ds) for j in range(ds)])
            ImSm /= ds**2*1.0
            Imout = common.ImFromNPArr(ImSm, oldtype)
        else:
            Imnp = Im.asnp()
            # trim Imnp so that its axes are divisible by ds
            Imnptrim = Imnp[0:Imnp.shape[0]/ds*ds, 0:Imnp.shape[1]/ds*ds,
                            0:Imnp.shape[2]/ds*ds]
            ImSm = sum([Imnptrim[i::ds, j::ds, k::ds]
                        for i in range(ds) for j in range(ds) for k in range(ds)])
            ImSm /= ds**3*1.0
            Imout = common.ImFromNPArr(ImSm, oldtype)
    else:                       # Field3Ds
        if Is2D(Im):
            Imbig = np.zeros((Im.grid().size().x/ds, Im.grid().size().y/ds, 3))
            for k in range(3):
                Imnp = Im.asnp()[k]
                # trim Imnp so that its axes are divisible by ds
                Imnptrim = Imnp[0:Imnp.shape[0]/ds*ds, 0:Imnp.shape[1]/ds*ds]
                ImSm = sum([Imnptrim[i::ds, j::ds]
                            for i in range(ds) for j in range(ds)])
                ImSm /= ds**2*1.0
                Imbig[:, :, k] = np.squeeze(ImSm)
            Imout = common.FieldFromNPArr(Imbig, oldtype)
        else:
            Imbig = np.zeros((Im.grid().size().x/ds, Im.grid().size().y/ds,
                              Im.grid().size().z/ds, 3))
            for k in range(3):
                Imnp = Im.asnp()[k]
                # trim Imnp so that its axes are divisible by ds
                Imnptrim = Imnp[0:Imnp.shape[0]/ds*ds, 0:Imnp.shape[1]/ds*ds,
                                0:Imnp.shape[2]/ds*ds]
                ImSm = sum([Imnptrim[i::ds, j::ds, k::ds]
                            for i in range(ds) for j in range(ds) for k in range(ds)])
                ImSm /= ds**3*1.0
                Imbig[:, :, k] = np.squeeze(ImSm)
            Imout = common.FieldFromNPArr(ImSm, oldtype)
    Imout.setSpacing(Im.spacing() * ds)
    Imout.setOrigin(Im.origin())  # origin remains the same (in world coords)

    Im.toType(oldtype)
    return Imout


def DownsampleGauss(Ob, newsize):
    '''returns a downsampled version of Ob onto the new size (list)'''

    mType = Ob.memType()
    if isinstance(newsize, ca.GridInfo):
        newsize = newsize.size().tolist()
    elif isinstance(newsize, ca.Vec3Di):
        newsize = newsize.tolist()

    if mType == ca.MEM_HOST:
        g = ca.GaussianFilterCPU()
    else:
        g = ca.GaussianFilterGPU()

    sigma = [None, None, None]
    newspacing = [None, None, None]
    for i in xrange(3):
        dsratio = Ob.size().tolist()[i]*1.0/newsize[i]
        newspacing[i] = Ob.spacing().tolist()[i]*dsratio
        if dsratio > 1.0:       # usual case
            # sigma[i] = dsratio/np.sqrt(2.0)  # this is slightly suspect
            sigma[i] = dsratio/2  # this is slightly suspect
        elif dsratio == 1.0:              # no downsampling
            sigma[i] = 0.1               # will do nothing, might help mem
        else:
            raise Exception("This is upsampling, not downsampling!")
    krad = [int(s*2.5) for s in sigma]

    g.updateParams(Ob.size(),
                   ca.Vec3Df(*sigma),
                   ca.Vec3Di(*krad))

    newOrigin = [Ob.origin().tolist()[i] + 0.5*(newspacing[i] - Ob.spacing().tolist()[i])
                 for i in xrange(3)]
    newGrid = ca.GridInfo(ca.Vec3Di(*newsize),
                          ca.Vec3Df(*newspacing),
                          ca.Vec3Df(*newOrigin))

    # do actual filtering
    if isinstance(Ob, ca.Field3D):
        raise NotImplementedError('Not yet implemented')
    Imfilt = ca.Image3D(Ob.grid(), mType)
    tmp = ca.Image3D(Ob.grid(), mType)
    g.filter(Imfilt, Ob, tmp)
    del tmp, Ob
    Obnew = ca.Image3D(newGrid, mType)
    ca.Resample(Obnew, Imfilt)

    return Obnew


def UpsampleH(h, newsize):
    '''given a small hgrid, makes a bigger, equivalent one'''

    # strategy:
    #   copy small v-field w/ unitary spacing
    #   resample the small v-field to a big v-field w/ unitary spacing
    #   multiply the big v-field by ratio (v(x) is now in voxels)
    #   copy to h, set the good grid

    oldsize = h.size().tolist()
    oldspacing = h.spacing().tolist()
    oldorigin = h.origin().tolist()

    v = h.copy()
    v.setSpacing(ca.Vec3Df(1.0, 1.0, 1.0))
    ca.HtoV_I(v)

    mType = h.memType()
    if isinstance(newsize, ca.GridInfo):
        newsize = newsize.size().tolist()
    elif isinstance(newsize, ca.Vec3Di):
        newsize = newsize.tolist()

    ratio = [1.0*newsize[i]/oldsize[i] for i in xrange(3)]
    newspacing = [round(oldspacing[i] * oldsize[i]/newsize[i], 4) for i in xrange(3)]
    if min(ratio) < 1.0:
        raise Exception("This is downsampling, not upsampling!")
    neworigin = [round(oldorigin[i] + 0.5*(newspacing[i] - oldspacing[i]), 4)
                 for i in xrange(3)]
    newgrid = ca.GridInfo(ca.Vec3Di(*newsize),
                          ca.Vec3Df(*newspacing),
                          ca.Vec3Df(*neworigin))
    out = ca.Field3D(ca.GridInfo(ca.Vec3Di(*newsize)), mType)
    ca.ResampleV(out, v, ca.BACKGROUND_STRATEGY_CLAMP)
    out *= ca.Vec3Df(*ratio)
    ca.VtoH_I(out)
    out.setGrid(newgrid)

    return out


def UpsampleH_I(h, newsize):
    '''upsample H in place (for multiscale, generally)'''
    out = UpsampleH(h, newsize)
    h.setGrid(out.grid())
    ca.Copy(h, out)


def UpsampleI(I, newsize):
    '''given a small I, makes a bigger, equivalent one'''

    oldsize = I.size().tolist()
    oldspacing = I.spacing().tolist()
    oldorigin = I.origin().tolist()

    mType = I.memType()
    if isinstance(newsize, ca.GridInfo):
        newsize = newsize.size().tolist()
    elif isinstance(newsize, ca.Vec3Di):
        newsize = newsize.tolist()

    ratio = [1.0*newsize[i]/oldsize[i] for i in xrange(3)]
    newspacing = [round(oldspacing[i] * oldsize[i]/newsize[i], 4) for i in xrange(3)]
    if min(ratio) < 1.0:
        raise Exception("This is downsampling, not upsampling!")
    neworigin = [round(oldorigin[i] + 0.5*(newspacing[i] - oldspacing[i]), 4)
                 for i in xrange(3)]
    newgrid = ca.GridInfo(ca.Vec3Di(*newsize),
                          ca.Vec3Df(*newspacing),
                          ca.Vec3Df(*neworigin))
    out = ca.Image3D(ca.GridInfo(ca.Vec3Di(*newsize)), mType)
    ca.Resample(out, I, ca.BACKGROUND_STRATEGY_CLAMP)
    out.setGrid(newgrid)

    return out


def UpsampleI_I(I, newsize):
    '''upsample I in place (for multiscale, generally)'''
    out = UpsampleI(I, newsize)
    I.setGrid(out.grid())
    ca.Copy(I, out)


def NumEl(Im):
    '''Returns the number of voxels'''
    return np.prod(Im.grid().size().tolist())


def Average(Im):
    '''Returns the average value of the Image3D'''
    return ca.Sum(Im)/NumEl(Im)


def NumNAN(Im):
    '''Returns the number of NANs in an Image'''
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    num = np.sum(np.isnan(Im.asnp()))
    Im.toType(oldType)
    return num


def NumINF(Im):
    '''Returns the number of INFs in an Image'''
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    num = np.sum(np.isinf(Im.asnp()))
    Im.toType(oldType)
    return num


def NumFinite(Im):
    '''Returns the number of Finite values in an Image'''
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    num = np.sum(np.isfinite(Im.asnp()))
    Im.toType(oldType)
    return num


def NanToNum(Im):
    '''Replace nan with zero and inf with finite nums'''
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    Im.asnp()[:, :, :] = np.nan_to_num(Im.asnp()[:, :, :])
    Im.toType(oldType)


def OffDim(Im):
    '''Returns the off-dimension a 2D Image3D

    if Im is size=[100, 1, 20], OffDim returns 'y'
    if Im is not 2D, OffDim fails'''
    [xsz, ysz, zsz] = [Im.grid().size().x,
                       Im.grid().size().y,
                       Im.grid().size().z]

    if xsz != 1 and ysz != 1 and zsz == 1:
        return 'z'
    elif xsz != 1 and ysz == 1 and zsz != 1:
        return 'y'
    elif xsz == 1 and ysz != 1 and zsz != 1:
        return 'x'
    else:
        raise Exception('Im is not 2D!')


def SelectPyCAGPU(deviceNum):
    '''sets cuda device given number'''
    ca.SetCUDADevice(deviceNum)


def SelectPyCUDAGPU(deviceNum):
    '''sets cuda device given number'''
    # import pycuda

    os.environ["CUDA_DEVICE"] = str(deviceNum)

    # import pycuda.driver as cuda

    # try:
    #     cuda.Context.pop()      # otherwise can't do twice in a row in ipython
    # except cuda.LogicError:
    #     pass
    import pycuda.autoinit
    # dev = cuda.Device(deviceNum)
    # dev.make_context()


def SelectGPU(deviceNum=None, pycuda=False):
    '''given deviceNum (cuda device number), selects that device for
    PyCA (and optionally PyCUDA) use.

    If a deviceNum isn't given, this smartly selects a GPU using
    nvidia-smi.

    This probably would not work with MPI because the first thread
    probably won't start using cuda fast enought for the second thread
    to recognize it.

    Note: this assumes all cuda-enabled GPUs are available to be used
    (unless we are on topaz, where it will only set cuda devices 0, 2,
    3, 4 (the TITAN Zs)

    *** This is hopefully not a problem with the new "SelectPyCUDAGPU"
    Note 2: there's a weird aspect of PyCA combined with PyCUDA: if
    only PyCA is being run and only the PyCA_GPU is set it will run
    completely on the selected GPU (at least all future mem-allocs
    will be done on the new gpu).  However, if SelectPyCUDAGPU() is
    run, there will be a small amount of memory (73-79MiB) allocated
    on the default GPU (cuda device 0 (possibly 1 on smi).

    This auto-selects GPUs based on the following criteria:
    1) selects the first completey free GPU
    2) selects device 0 if it looks like there are only PyCUDA dummy processes
    3) selects device using the least amount of memory
    '''

    if deviceNum is not None:
        SelectPyCAGPU(deviceNum)
        if pycuda:
            SelectPyCUDAGPU(deviceNum)
        return

    if os.environ['HOSTNAME'] not in ['test.sci.utah.edu',   # ssh to topaz
                                      'topaz',                # normal shell
                                      'topaz.sci.utah.edu']:   # other?
        print "Warning! Not on topaz, assuming all cuda devices valid"
        cuda_devs = range(ca.GetNumberOfCUDADevices())
        smi_devs = range(ca.GetNumberOfCUDADevices())
    else:
        # Cuda devices [0, 2, 3, 4] refers to [1, 2, 3, 4] in nvidia-smi
        # cuda_devs = [0, 2, 3, 4]
        # smi_devs = [1, 2, 3, 4]
        cuda_devs = [4, 2, 3, 0]
        smi_devs = [4, 2, 3, 1]
    # Choose selected device
    st = subprocess.check_output("nvidia-smi")

    si = st.rfind("Process name")
    process_lines = st[si:-1].split('\n')[2:-1]

    gpu_nums = []
    mem_vals = []
    pids = []
    for line in process_lines:
        i = line.rfind('MiB')
        if i == -1:
            continue
        gpu_nums.append(int(line[5]))
        pids.append(int(line[6:17]))
        mem_vals.append(int(line[i-5:i]))

    if os.getpid() in pids:
        print "SelectGPU(): GPU already chosen, not changing"
        # print os.getpid(), 'is in', pids
        return

    # first round: try to find gpu that isn't used
    for i, (cd, sd) in enumerate(zip(cuda_devs, smi_devs)):
        # print i, cd, sd
        if sd not in gpu_nums:
            print 'SelectGPU(): GPU ', sd, 'is free; selecting'
            SelectPyCAGPU(cd)
            if pycuda:
                SelectPyCUDAGPU(cd)
            return

    maxmem0 = 0
    totmems = {smigpu: 0 for smigpu in smi_devs}  # dict of memsums
    for i, (smigpu, mem) in enumerate(zip(gpu_nums, mem_vals)):
        if smigpu == smi_devs[0]:
            maxmem0 = max(maxmem0, mem)
        totmems[smigpu] += mem
    # second round - if all process on gpu0 use < 100, assume it is free
    if maxmem0 < 100:
        print 'Only dummy processes found on GPU 0, stay on default GPU'
        return
    else:  # third round: choose process that uses the least amount of memory
        sd = min(totmems, key=totmems.get)
        cd = cuda_devs[smi_devs.index(sd)]

        print ('All CUDA devices occupied. '
               'Cuda device {} has the least used memory. Selecting.'.format(sd))
        SelectPyCAGPU(cd)
        if pycuda:
            SelectPyCUDAGPU(cd)


def mkdir(dirname):
    try:
        os.makedirs(dirname)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else:
            raise


def ComposeVFields(vflist):
    '''Given a list of vfields vflist = [v0, v1, v2]
    returns a composed vfield
    vout = v2(v1(v0(x)))'''

    vout = ca.Field3D(vflist[0].grid(), vflist[0].memType())
    vtmp = ca.Field3D(vflist[0].grid(), vflist[0].memType())
    ca.SetToZero(vout)
    ca.SetToZero(vtmp)
    for v in vflist:
        ca.ApplyV(vout, v, vtmp, ca.BACKGROUND_STRATEGY_ZERO)
        ca.Copy(vtmp, vout)

    return vout


def ComposeHFields(hflist):
    '''Given a list of hfields hflist = [h0, h1, h2]
    returns a composed hfield
    hout = h2(h1(h0(x)))'''

    hout = ca.Field3D(hflist[0].grid(), hflist[0].memType())
    htmp = ca.Field3D(hflist[0].grid(), hflist[0].memType())
    ca.SetToIdentity(hout)
    ca.SetToIdentity(htmp)
    for h in hflist:
        ca.ApplyH(hout, h, htmp, ca.BACKGROUND_STRATEGY_ID)
        ca.Copy(htmp, hout)

    return hout


def InsertSlice(Im3D, Im2D, sliceIdx, dim='z'):
    '''Opposite of ExtractSlice.  Places the 2D Image Im2D
    in Im3D.  If dim == 'z', then
    Im3D[:, :, sliceIdx] = Im2D'''

    oldType = Im2D.memType()
    Im2D.toType(Im3D.memType())

    if dim == 'z':
        start = ca.Vec3Di(0, 0, sliceIdx)
    elif dim == 'y':
        start = ca.Vec3Di(0, sliceIdx, 0)
    else:
        start = ca.Vec3Di(sliceIdx, 0, 0)

    ca.SetSubVol_I(Im3D, Im2D, start)
    Im2D.toType(oldType)


def GetAspect(grid, dim='z', axis='default', retFloat=True):
    '''given a image grid, determines the 2D aspect ratio of the
    off-dimension'''

    imsz = grid.size().tolist()
    # aspect is always (displayed) height spacing/width spacing
    # sz is displayed (displayed, apparent) [width, height]
    if dim == 'x':
        aspect = grid.spacing().y/grid.spacing().z
        sz = [imsz[2], imsz[1]]
    elif dim == 'y':
        aspect = grid.spacing().x/grid.spacing().z
        sz = [imsz[2], imsz[0]]
    else:
        aspect = grid.spacing().x/grid.spacing().y
        sz = [imsz[1], imsz[0]]

    if axis == 'cart':
        aspect = 1.0/aspect
        sz = sz[::-1]

    if retFloat:
        return aspect

    if aspect > 1:
        scale = [sz[0]/aspect, sz[1]*1.0]
    else:
        scale = [sz[0]*1.0, sz[1]*aspect]

    # scale incorporates image size (grow if necessary) and aspect ratio
    while scale[0] <= 400 and scale[1] <= 400:
        scale = [scale[0]*2, scale[1]*2]

    return [int(round(scale[0])), int(round(scale[1]))]


def ToField(im):
    '''returns a Field3D version of an Image3D im.  This is a hack for
    PyCA operators that dont successfully implement Field3D-Image3D
    operations. In that case you can simply implement a image3d as a
    field and then do the operations'''

    f = ca.Field3D(im.grid(), im.memType())
    ca.Copy(f, im, 0)
    ca.Copy(f, im, 1)
    ca.Copy(f, im, 2)
    return f


def ToFastType(ob):
    '''takes an object and puts the object on the GPU'''
    ob.toType(ca.MEM_DEVICE)


def ToSlowType(ob):
    '''takes an object and puts the object on the GPU'''
    ob.toType(ca.MEM_HOST)


def MakeGrid(size, spacing=None, origin=None):
    '''generate a gridInfo object given size, spacing (optional), and
    origin (optional)

    size, spacing, and origin should each be a PyCa Vec3Ds, number, or
    indexable

    origin can also be the string 'center', which sets the center of
    the image as the origin '''

    # convert size/spacing/origin to Vec3Ds
    if IsNumber(size):
        size = ca.Vec3Di(size, size, size)
    elif isinstance(size, ca.Vec3Di):
        pass
    elif isinstance(size, ca.GridInfo):
        return size.copy()      # just return a copy of the grid
    else:
        try:
            size = ca.Vec3Di(size[0], size[1], size[2])
        except AttributeError:
            raise Exception("Invalid size:", size)

    # make sure size is valid:
    if size.x < 1 or size.y < 1 or size.z < 1:
        raise Exception("Size values should be positive")

    # spacing
    if spacing is None:
        spacing = ca.Vec3Df(1.0, 1.0, 1.0)
    elif IsNumber(spacing):
        spacing = ca.Vec3Df(spacing, spacing, spacing)
    elif isinstance(spacing, ca.Vec3Df):
        pass
    elif isinstance(spacing, str) and spacing.lower() == 'center':   # allow this
        spacing = ca.Vec3Df(1.0, 1.0, 1.0)
        origin = 'center'
    else:
        try:
            spacing = ca.Vec3Df(spacing[0], spacing[1], spacing[2])
        except AttributeError:
            raise Exception("invalid spacing:", spacing)

    # make sure spacing is valid:
    if spacing.x <= 0.0 or spacing.y <= 0 or spacing.z <= 0:
        raise Exception("spacing values should be positive")

    # origin
    if origin is None:
        origin = ca.Vec3Df(0.0, 0.0, 0.0)
    elif IsNumber(origin):
        origin = ca.Vec3Df(origin, origin, origin)
    elif isinstance(origin, ca.Vec3Df):
        pass
    elif isinstance(origin, str) and origin.lower() == 'center':
        origin = ca.Vec3Df((1.0-size.x)/2.0*spacing.x,
                           (1.0-size.y)/2.0*spacing.y,
                           (1.0-size.z)/2.0*spacing.z)
    else:
        try:
            origin = ca.Vec3Df(origin[0], origin[1], origin[2])
        except AttributeError:
            raise Exception("invalid origin:", origin)

    return ca.GridInfo(size, spacing, origin)


def GenRandomVDef(grid, mType=ca.MEM_DEVICE, maxV=2.5, maxT=1.5, fluidParams=None, nLap=2):
    '''generates a random smooth vector field
    maxV is the largest magnitude of the deformation in a direction (not including translation)
    maxT is the largest magnitude of the translation in a direction'''

    grid = MakeGrid(grid)

    v = ca.Field3D(grid, mType)
    ca.SetMem(v, 0.0)

    v.toType(ca.MEM_HOST)
    v.asnp()[0][:, :, :] = np.random.randn(grid.size().x, grid.size().y, grid.size().z)
    v.asnp()[1][:, :, :] = np.random.randn(grid.size().x, grid.size().y, grid.size().z)
    if grid.size().z != 1:
        v.asnp()[2][:, :, :] = np.random.randn(grid.size().x, grid.size().y, grid.size().z)

    v.toType(mType)

    if fluidParams is not None:
        alpha, beta, gamma = fluidParams
    else:
        alpha, beta, gamma = 1, 0, .0001

    if mType == ca.MEM_HOST:
        diffOp = ca.FluidKernelFFTCPU()
    else:
        diffOp = ca.FluidKernelFFTGPU()

    diffOp.setAlpha(alpha)
    diffOp.setBeta(beta)
    diffOp.setGamma(gamma)
    diffOp.setGrid(grid)

    v -= ca.SumComp(v)/v.nVox()  # zero-mean
    for _ in xrange(nLap):
        diffOp.applyInverseOperator(v)
        v -= ca.SumComp(v)/v.nVox()  # zero-mean

    mm = []
    tmp = ca.Image3D(v.grid(), v.memType())
    for i in xrange(3):
        ca.Copy(tmp, v, i)
        mm.append(ca.MinMax(tmp))
    maxdiff = max(mx-mn for mn, mx in mm)
    v /= maxdiff
    v *= maxV

    Trand = np.random.randn(3)
    if grid.size().z == 1:
        Trand[2] = 0

    Trand /= max(abs(t) for t in Trand)
    Trand *= maxT
    Tsub = ca.SumComp(v)/v.nVox()
    v -= Tsub + ca.Vec3Df(*Trand)

    return v


def SplatWorld(f_out, f_in, bg=ca.BACKGROUND_STRATEGY_CLAMP):
    '''ca.SplatWorld, but also works for Field3Ds
    doesn't work with weights (yet, at least)
    '''

    if isinstance(f_in, ca.Image3D):
        ca.SplatWorld(f_out, f_in, bg)
        return

    i_out = ca.Image3D(f_out.grid(), f_out.memType())
    i_in = ca.Image3D(f_in.grid(), f_in.memType())

    for i in xrange(3):
        ca.Copy(i_in, f_in, i)
        ca.SplatWorld(i_out, i_in, bg)
        ca.Copy(f_out, i_out, i)


def ResampleWorld(f_out, f_in, bg=ca.BACKGROUND_STRATEGY_CLAMP):
    '''ca.ResampleWorld, but also works for Field3Ds
    '''

    if isinstance(f_in, ca.Image3D):
        ca.ResampleWorld(f_out, f_in, bg)
        return

    i_out = ca.Image3D(f_out.grid(), f_out.memType())
    i_in = ca.Image3D(f_in.grid(), f_in.memType())

    for i in xrange(3):
        ca.Copy(i_in, f_in, i)
        ca.ResampleWorld(i_out, i_in, bg)
        ca.Copy(f_out, i_out, i)


def ImtoField(im, dim=None):
    '''takes an Image3D and returns the corresponding Field3D
    f = [im, im, im]
    if the input is already a Field3D, returns the original

    if the 'dim' paremeter is set, it will just copy the image to that
    field direction (helpful for making red green or blue image)
    '''
    if isinstance(im, ca.Field3D):
        return im

    f = ca.Field3D(im.grid(), im.memType())
    if dim is None:
        ca.Copy(f, im, 0)
        ca.Copy(f, im, 1)
        ca.Copy(f, im, 2)
    else:
        ca.SetMem(f, 0.0)
        ca.Copy(f, im, dim)

    return f


def BlendObjs(ob0, ob1, c=0.5):
    '''returns a blend of two objects:
    blend = c*ob0 + (1-c)*ob1

    if ob0 and ob1 aren't both Field3D's or Image3Ds,
    returns a Field3D
    '''

    assert 0.0 <= c <= 1.0

    if not (isinstance(ob0, ca.Image3D) and isinstance(ob1, ca.Image3D)):
        blend = ca.Field3D(ob0.grid(), ob0.memType())
        # (possibly) convert to field
        ob0 = ImtoField(ob0)
        ob1 = ImtoField(ob1)
    else:
        blend = ca.Image3D(ob0.grid(), ob0.memType())

    ca.MulC_Add_MulC(blend, ob0, c, ob1, 1-c)

    return blend


def ExtractSlice(ob, sliceIdx=None, dim='z'):
    ''' Extract Slice, but works for Im's or VF's'''
    import PyCA.Common as common
    if isinstance(ob, ca.Image3D):
        return common.ExtractSliceIm(ob, sliceIdx, dim)
    elif isinstance(ob, ca.Field3D):
        return common.ExtractSliceVF(ob, sliceIdx, dim)
    else:
        raise Exception('not an Image3D or Field3D')


def SwapAxes(ob, axis1, axis2):
    '''returns a new Image3D/Field3D that has the two axes
    swapped'''
    import PyCA.Common as common

    oldType = ob.memType()
    ob.toType(ca.MEM_HOST)

    sp = [ob.spacing().x, ob.spacing().y, ob.spacing().z]
    orig = [ob.origin().x, ob.origin().y, ob.origin().z]

    # convert to numpy and do the swapaxes there
    if isinstance(ob, ca.Image3D):
        ob_copy = np.swapaxes(ob.asnp(), axis1, axis2).copy()
        ob_out = common.ImFromNPArr(ob_copy, mType=oldType)
    else:
        im0_copy = np.swapaxes(ob.asnp()[0], axis1, axis2).copy()
        im1_copy = np.swapaxes(ob.asnp()[1], axis1, axis2).copy()
        im2_copy = np.swapaxes(ob.asnp()[2], axis1, axis2).copy()

        # add a 4th axis to all images so we can concat
        ims = [im[..., np.newaxis] for im in [im0_copy, im1_copy, im2_copy]]
        ob_out = common.FieldFromNPArr(np.concatenate(ims, axis=3), mType=oldType)

    # flip origin and spacing (size will take care of itself)
    sp[axis1], sp[axis2] = sp[axis2], sp[axis1]
    orig[axis1], orig[axis2] = orig[axis2], orig[axis1]
    ob_out.setSpacing(ca.Vec3Df(sp[0], sp[1], sp[2]))
    ob_out.setOrigin(ca.Vec3Df(orig[0], orig[1], orig[2]))

    ob.toType(oldType)
    return ob_out


def FlipAxis(ob, axis):
    '''returns a new Image3D/Field3D that has the axis flipped
    equivalent to Im = Im[::-1, :, :] for axis=0, etc.

    does not change the origin for that axis
    '''
    import PyCA.Common as common

    oldType = ob.memType()
    ob.toType(ca.MEM_HOST)

    # convert to numpy and do the swapaxes there
    if isinstance(ob, ca.Image3D):
        ob_copy = np.swapaxes(np.swapaxes(ob.asnp(), 0, axis)[::-1], 0, axis).copy()
        ob_out = common.ImFromNPArr(ob_copy, mType=oldType)
    else:
        im0_copy = np.swapaxes(np.swapaxes(ob.asnp()[0], 0, axis)[::-1], 0, axis).copy()
        im1_copy = np.swapaxes(np.swapaxes(ob.asnp()[1], 0, axis)[::-1], 0, axis).copy()
        im2_copy = np.swapaxes(np.swapaxes(ob.asnp()[2], 0, axis)[::-1], 0, axis).copy()

        # add a 4th axis to all images so we can concat
        ims = [im[..., np.newaxis] for im in [im0_copy, im1_copy, im2_copy]]
        ob_out = common.FieldFromNPArr(np.concatenate(ims, axis=3), mType=oldType)

    ob_out.setGrid(ob.grid())

    ob.toType(oldType)
    return ob_out


def ReorderAxes(ob, order):
    '''returns an Image/Field3D with reordered axes, given by 'order'.
    For example, if order = ['z', 'x', '-y'], the new object is
    I(z, x, -y)

    You basically decide which axis moves, so order=['y', '-x', 'z'] means
    x goes to positve y, y goes to negative x, z stays the same.
    '''

    if len(order) == 2 and sorted(order)[-1][-1] == 'y':
        order = [order[0], order[1], ['z']]

    flipaxis = [idx[0] == '-' for idx in order]

    chartoint = {'x': 0,
                 'y': 1,
                 'z': 2}

    # change order to 0/1/2
    order = [chartoint[ch[-1]] for ch in order]

    # negate things first
    for i, bl in enumerate(flipaxis):
        if bl:
            ob = FlipAxis(ob, i)

    # just enumerate the options
    if order == [0, 1, 2]:
        pass
    elif order == [0, 2, 1]:
        ob = SwapAxes(ob, 1, 2)
    elif order == [1, 0, 2]:
        ob = SwapAxes(ob, 0, 1)
    elif order == [1, 2, 0]:
        ob = SwapAxes(ob, 0, 2)
        ob = SwapAxes(ob, 1, 2)
    elif order == [2, 0, 1]:
        ob = SwapAxes(ob, 0, 1)
        ob = SwapAxes(ob, 1, 2)
    elif order == [2, 1, 0]:
        ob = SwapAxes(ob, 0, 2)
    else:
        raise NotImplementedError('invalid order')

    return ob


def MatrixMul(fout, A, f):
    '''multiplies a field by a matrix:

    fout = A.f

    A can be a 2x2 or 3x3 matrix
    '''

    dim = len(A)

    if dim not in [2, 3]:
        raise NotImplementedError('Only for 2D or 3D multiplications')

    fim = ca.ManagedImage3D(f.grid(), f.memType())
    imtmp = ca.ManagedImage3D(f.grid(), f.memType())

    if dim == 2:
        ca.Copy(fout, f)            # for 2D, ignore dimension 3

    ca.SetMem(imtmp, 0.0)

    for i in xrange(dim):
        ca.SetMem(imtmp, 0.0)
        for j in xrange(dim):
            ca.Copy(fim, f, j)
            ca.Add_MulC_I(imtmp, fim, A[i, j])  # imtmp += Aij*fim
        ca.Copy(fout, imtmp, i)


def MatrixMul_I(A, f):
    '''multiplies a field by a matrix:

    f = A.f

    A can be a 2x2 or 3x3 matrix
    '''
    fout = f.copy()
    MatrixMul(fout, A, f)
    ca.Copy(f, fout)


def MatrixMulMem(fout, A, f):
    '''same as MatrixMul, but only uses 1 temp GPU field (to save memory)

    it might be slightly faster than doing the matrix mul on the CPU'''

    dim = len(A)
    if dim not in [2, 3]:
        raise NotImplementedError('Only for 2D or 3D multiplications')

    if dim == 2:
        ca.Copy(fout, f)            # for 2D, ignore dimension 3

    grid = f.grid()
    mType = f.memType()

    fout.toType(ca.MEM_HOST)
    foutims = [ca.ManagedImage3D(grid, ca.MEM_HOST) for _ in xrange(dim)]

    fims = [ca.ManagedImage3D(grid, mType) for _ in xrange(dim)]
    for i in xrange(dim):
        ca.Copy(fims[i], f, i)
    f.toType(ca.MEM_HOST)

    imtmp = ca.ManagedImage3D(f.grid(), mType)

    for i in xrange(dim):
        ca.SetMem(imtmp, 0.0)
        for j in xrange(dim):
            ca.Add_MulC_I(imtmp, fims[j], A[i, j])  # imtmp += Aij*fim
        foutims[i].toType(mType)
        ca.Copy(foutims[i], imtmp)
        if i < dim - 1:
            foutims[i].toType(ca.MEM_HOST)
    del imtmp
    del fims
    fout.toType(mType)
    for i in reversed(xrange(dim)):
        foutims[i].toType(mType)
        ca.Copy(fout, foutims[i], i)
    del foutims

    f.toType(mType)


def SetToRealIdentity(f):
    '''like ca.SetToIdentity(f), except it sets it to the
    identity in world coordinates'''
    ca.SetToIdentity(f)
    ca.MulC_I(f, f.grid().spacing())
    ca.AddC_I(f, f.grid().origin())


def HtoReal(h, grid=None):
    '''Given an index-valued h-field, converst to the appropriate
    real-valued h-field. You can optionally supply a grid
    for the conversion. Otherwise, it will used the h-field's
    grid
    '''

    if grid is None:
        grid = h.grid()

    h *= grid.spacing()
    h += grid.origin()


def HtoIndex(h, grid=None):
    '''Given an real-valued h-field, converst tothe appropriate
    index-valued h-field. You can optionally supply a grid
    for the conversion. Otherwise, it will used the h-field's
    grid
    '''

    if grid is None:
        grid = h.grid()

    h -= grid.origin()
    h /= grid.spacing()


def ApplyHReal(ob_out, ob_in, h, bg=ca.BACKGROUND_STRATEGY_PARTIAL_ZERO):
    '''applies a deformation with a real-valued h-field

    Typically apply h works as:
    I_out(x) = I_in(h(x))

    where h is valued by the index values of I_in
    Here we apply it when h is valued by the real coordinate
    values of I_in
    '''

    HtoIndex(h, ob_in.grid())
    ca.ApplyH(ob_out, ob_in, h, bg)
    HtoReal(h, ob_in.grid())    # convert back


def MemInfo(mm=None):
    '''prints the info about how much managed memory is being used

    (defaults to the global memory manager)'''
    if mm is None:
        mm = ca.ThreadMemoryManager.instance()
    print 'Memory:', mm.getNumLockedPools(), 'of', mm.getNumPools()


def FFTSize2(size):
    '''given a 3D size, returns a good size to use (low prime factors)

    every dimension of the returned size will only have factors <= 7
    '''
    # max of 2048
    good_numbers = [2**i2*3**i3*5**i5*7**i7 for i2 in xrange(1, 12)
                    for i3 in xrange(8) for i5 in xrange(6) for i7 in xrange(5)
                    if 2**i2*3**i3*5**i5*7**i7 < 2048]

    good_numbers = sorted(good_numbers)

    szx = next(i for i in good_numbers if i >= size[0])
    szy = next(i for i in good_numbers if i >= size[1])
    szz = next(i for i in good_numbers if i >= size[2])

    return [szx, szy, szz]


def factors(n):
    '''return list of factors for an integer'''
    i = 2
    factorsls = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factorsls.append(i)
    if n > 1:
        factorsls.append(n)
    return factorsls


def FFTSize(size):
    '''given a 3D size, returns a good size to use (based on tests)
    '''
    # orig up to 396
    # good_numbers = [1, 6, 10, 12, 20, 24, 30, 36, 40, 42, 48, 50, 54, 56,
    #                 60, 64, 66, 72, 80, 84, 90, 96, 100, 108, 110, 112,
    #                 128, 132, 144, 160, 176, 180, 192, 196, 200, 216,
    #                 256, 264, 270, 288, 294, 300, 320, 324, 352, 360,
    #                 384, 392, 396]

    # 1D test
    # good_numbers2 = [1, 6, 10, 12, 20, 24, 30, 32, 36, 40, 42, 48, 50, 54, 56,
    #                  64, 72, 80, 84, 90, 96, 100, 108, 110, 112,
    #                  128, 132, 144, 160, 176, 180, 192, 196, 200, 216,
    #                  256, 407, 409, 415, 417, 419, 422, 423, 425, 427,
    #                  431, 433, 435, 437, 439, 446, 447, 449, 451, 454,
    #                  455, 457, 458, 459, 463, 465, 466, 467, 469, 471,
    #                  478, 479, 481, 482, 483, 485, 487, 489, 491, 495,
    #                  497, 499, 502, 503, 511]

    # 2D test
    # good_numbers = [1, 6, 12, 24, 30, 36, 42, 72, 112, 140, 144, 168,
    #                 176, 200, 210, 216, 256, 264, 270, 280, 288, 300,
    #                 320, 324, 336, 352, 360, 384, 392, 432, 512]

    # # i get weird results > 400, and 366 is max for DIR
    # good_numbers = [1, 6, 12, 20, 24, 30, 36, 40, 42, 48, 50, 56,
    #                 60, 64, 72, 80, 84, 90, 96, 100, 108, 110, 112,
    #                 128, 132, 144, 160, 176, 180, 192, 196, 200, 256,
    #                 407, 409, 415, 417, 419, 422, 423, 425, 427, 431,
    #                 433, 435, 437, 439, 446, 447, 449, 451, 454, 455,
    #                 457, 458, 459, 463, 465, 466, 467, 469, 471, 478,
    #                 479, 481, 482, 483, 485, 487, 489, 491, 495, 497,
    #                 499, 502, 503, 511, 512]
    # good_numbers = [1, 6, 12, 20, 24, 30, 36, 40, 42, 48, 50, 56,
    #                 64, 72, 80, 84, 90, 96, 100, 108, 110, 112,
    #                 128, 132, 144, 160, 176, 180, 192, 196, 200, 216, 256,
    #                 407, 409, 415, 417, 419, 422, 423, 425, 427, 431,
    #                 433, 435, 437, 439, 446, 447, 449, 451, 454, 455,
    #                 457, 458, 459, 463, 465, 466, 467, 469, 471, 478,
    #                 479, 481, 482, 483, 485, 487, 489, 491, 495, 497,
    #                 499, 502, 503, 505, 511]

    # sticking with this...
    # good_numbers = [1, 6, 12, 20, 24, 30, 36, 40, 42, 48, 50, 56, 60,
    #                 64, 72, 80, 84, 90, 96, 100, 108, 110, 112, 128,
    #                 132, 144, 160, 176, 180, 192, 196, 200, 256, 264,
    #                 270, 288, 294, 300, 320, 324, 352, 360, 384, 392, 400]
    good_numbers = [1, 6, 12, 20, 24, 30, 36, 40, 42, 48, 50, 56,
                    64, 72, 80, 84, 90, 96, 100, 108, 110, 112, 128,
                    132, 144, 160, 176, 180, 192, 196, 200, 216, 256, 264,
                    270, 288, 294, 300, 320, 324, 352, 360, 384, 392, 400]

    size_fallback = FFTSize2(size)

    # for i in good_numbers:
    #     print i, factors(i)

    szx = next((i for i in good_numbers if i >= size[0]), size_fallback[0])
    szy = next((i for i in good_numbers if i >= size[1]), size_fallback[1])
    szz = next((i for i in good_numbers if i >= size[2]), size_fallback[2])

    return [szx, szy, szz]


def testFFTsize(size, N=None):
    '''silly function to test sizes for doing the FFT inverse a bunch of times'''
    if N is None:
        N = max(10, int(500000000.0/np.prod(size)))
    size1 = FFTSize2(size)      # prime method
    size2 = FFTSize(size)       # tested method

    print "running for {} iterations".format(N)

    for s in [size, size1, size2, size2, size1, size]:
        grid = ca.GridInfo(ca.Vec3Di(*s))
        print grid
        f = ca.Field3D(grid, ca.MEM_DEVICE)
        diffOp = ca.FluidKernelFFTGPU()
        diffOp.setGrid(grid)
        ca.SetMem(f, 0.0)
        ca.Sum(f)

        t = time.time()
        for _ in xrange(N):
            diffOp.applyInverseOperator(f)
        ca.Sum(f)
        print "  {} seconds".format(time.time()-t)

        del diffOp, f

# def InvertHField(h):
#     '''returns an inverted h using numpy's meshgrid'''
#     mType = h.memType()

def printinfo(it, nIters, energy, startTime=None, lastTime=None, verbose=2):
    '''given the current iteration, prints a summary

    Note: energy should be given as [reg_energy, diff_energy, tot_energy]
    where reg_energy is a list or a float.
    '''

    if verbose == 1:
        if nIters <= 10 or it % (nIters/10) == 0 or it >= nIters-1 or it < 0:
            pass
        else:
            return
    elif verbose == 0:
        return

    Initial = it < 0
    Final = it >= nIters
    if Initial or Final:
        it = 0                  # just for spacing

    printstr = 'Iter ' + str(it).rjust(len(str(nIters-1))) + '/' + str(nIters-1)
    if Initial:
        printstr = 'Initial:'.ljust(len(printstr))
    elif Final:
        printstr = 'Final:'.ljust(len(printstr))

    if startTime is not None:
        printstr += ' : {0} :'.format(
            datetime.timedelta(seconds=round(time.time()-startTime)))
    if lastTime is not None:
        printstr += ' : ({0}) '.format(
            datetime.timedelta(seconds=round(time.time()-lastTime)))

    # if energy is a long list of lists ( energy[entype][it] )
    try:
        energy[0][0]
        energy = [en[-1] for en in energy] # now energy is a list of float(s)
    except IndexError:                     # energy value doesn't exist
        print printstr
        return
    except TypeError:
        pass

    if len(energy) == 3:
        printstr += ' E (sigma*Reg + Data): {0:.5e} + {1:.5e} = {2:.5e}'.format(
            energy[0], energy[1], energy[2])
    else:
        printstr += ' E = {0:.5e}'.format(energy[-1])

    print printstr
