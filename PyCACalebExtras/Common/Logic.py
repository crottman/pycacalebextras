'''Logic and Indexing functions for PyCA'''
import PyCA.Core as ca


def Not(Io, Ii):
    '''performs a binary NOT of a 0 and 1 valued Image3D

    Io = !Ii ( = -Ii+1)

    '''
    ca.Neg(Io, Ii)
    ca.AddC_I(Io, 1.0)


def Not_I(Im):
    '''performs a binary NOT of a 0 and 1 valued Image3D

    Io = !Ii ( = -Ii+1)

    '''
    ca.Neg_I(Im)
    ca.AddC_I(Im, 1.0)


def SetRegionGT(I1, I2, A, B, scratchI=None):
    '''I1(I2 > A) = B

    I1, I2 are Image3D's and can be identical.
    A, B are either Image3D's or constants
    scratchI is an optional scratch Image3D
    '''

    if scratchI is None:
        scratchI = ca.Image3D(I1.grid(), I1.memType())
    ca.GT(scratchI, I2, A)
    Not_I(scratchI)
    I1 *= scratchI
    Not_I(scratchI)
    scratchI *= B
    ca.Add_I(I1, scratchI)


def SetRegionGTE(I1, I2, A, B, scratchI=None):
    '''I1(I2 >= A) = B

    I1, I2 are Image3D's and can be identical.
    A, B are either Image3D's or constants
    scratchI is an optional scratch Image3D
    '''

    if scratchI is None:
        scratchI = ca.Image3D(I1.grid(), I1.memType())
    ca.GTE(scratchI, I2, A)
    Not_I(scratchI)
    I1 *= scratchI
    Not_I(scratchI)
    scratchI *= B
    ca.Add_I(I1, scratchI)


def SetRegionEQ(I1, I2, A, B, scratchI=None):
    '''I1(I2 == A) = B

    I1, I2 are Image3D's and can be identical.
    A, B are either Image3D's or constants
    scratchI is an optional scratch Image3D
    '''

    if scratchI is None:
        scratchI = ca.Image3D(I1.grid(), I1.memType())
    ca.EQ(scratchI, I2, A)
    Not_I(scratchI)
    I1 *= scratchI
    Not_I(scratchI)
    scratchI *= B
    ca.Add_I(I1, scratchI)


def SetRegionNEQ(I1, I2, A, B, scratchI=None):
    '''I1(I2 != A) = B

    I1, I2 are Image3D's and can be identical.
    A, B are either Image3D's or constants
    scratchI is an optional scratch Image3D
    '''

    if scratchI is None:
        scratchI = ca.Image3D(I1.grid(), I1.memType())
    ca.NEQ(scratchI, I2, A)
    Not_I(scratchI)
    I1 *= scratchI
    Not_I(scratchI)
    scratchI *= B
    ca.Add_I(I1, scratchI)


def SetRegionLT(I1, I2, A, B, scratchI=None):
    '''I1(I2 < A) = B

    I1, I2 are Image3D's and can be identical.
    A, B are either Image3D's or constants
    scratchI is an optional scratch Image3D
    '''

    if scratchI is None:
        scratchI = ca.Image3D(I1.grid(), I1.memType())
    ca.LT(scratchI, I2, A)
    Not_I(scratchI)
    I1 *= scratchI
    Not_I(scratchI)
    scratchI *= B
    ca.Add_I(I1, scratchI)


def SetRegionLTE(I1, I2, A, B, scratchI=None):
    '''I1(I2 <= A) = B

    I1, I2 are Image3D's and can be identical.
    A, B are either Image3D's or constants
    scratchI is an optional scratch Image3D
    '''

    if scratchI is None:
        scratchI = ca.Image3D(I1.grid(), I1.memType())
    ca.LTE(scratchI, I2, A)
    Not_I(scratchI)
    I1 *= scratchI
    Not_I(scratchI)
    scratchI *= B
    ca.Add_I(I1, scratchI)


def SetRegion(im_out, im, region, A):
    '''Given a region, set it to a float val/Image3D (A), and keeps
    the rest of the image the same

    im_out(region) = A    ( = Im(region) or c)
    im_out(!region) = im(!region)

    '''

    from .Common import BinaryInverse

    mType = im.memType()
    grid = im.grid()
    im_rng = ca.Image3D(grid, mType)
    im_irng = ca.Image3D(grid, mType)
    iregion = ca.Image3D(grid, mType)
    BinaryInverse(iregion, region)
    # ca.MulC(im_rng, region, A)
    # ca.Mul(im_rng, region, A)
    ca.Copy(im_rng, region)
    im_rng *= A
    ca.Mul(im_irng, iregion, im)
    ca.Add(im_out, im_rng, im_irng)


def SetRegion_I(im, region, A):
    '''Given a region, set it to a float val/Image3D (A), and keeps
    the rest of the image the same

    im(region) = A   ( = A_im(region) or c )
    im(!region) = im(!region)

    '''
    SetRegion(im, im, region, A)
