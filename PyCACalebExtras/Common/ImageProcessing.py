'''Logic and Indexing functions for PyCA'''
import PyCA.Core as ca
import numpy as np
from . import Common as cc


def SetGaussianFilter(Im, sigma):
    '''given an image (that will be filtered), creates a
    gaussian filter. Selects CPU/GPU and kernel size and returns
    the filter
    '''
    if cc.IsNumber(sigma):
        sigma = [sigma, sigma, sigma]

    if Im.memType() == ca.MEM_DEVICE:
        g = ca.GaussianFilterGPU()
    else:
        g = ca.GaussianFilterCPU()

    if Im.size().z > 1:
        g.updateParams(Im.size(), ca.Vec3Df(sigma[0], sigma[1], sigma[2]),
                       ca.Vec3Di(int(sigma[0]*3), int(sigma[1]*3), int(sigma[2]*3)))
    else:
        g.updateParams(Im.size(), ca.Vec3Df(sigma[0], sigma[1], 1),
                       ca.Vec3Di(int(sigma[0]*3), int(sigma[1]*3), 1))
    return g


def VarianceEqualize(I_out, Im, sigma=5.0, eps=1e-3):
    '''Takes an Image and gives the variance equalized version.

    I_out, Im: PyCA Image3Ds
    sigma: (scalar) gaussian filter parameter
    eps: (scalar) division regularizer

    sigma is the width (in voxels) of the gaussian kernel
    eps is the regularizer

    for a gaussian kernel k, we have

    I_ve = I'/sqrt(k * I'^2)
    where I' = I - k*I

    '''

    # if sigma is just a number (generally true)
    if isinstance(sigma, (int, long, float)):
        sigma = [sigma, sigma, sigma]

    scratchI = ca.Image3D(Im.grid(), Im.memType())
    Ibar = ca.Image3D(Im.grid(), Im.memType())
    I3 = ca.Image3D(Im.grid(), Im.memType())

    g = SetGaussianFilter(I_out, sigma)

    g.filter(Ibar, Im, scratchI)
    ca.Sub(Ibar, Im, Ibar)

    g.filter(I3, Ibar**2, scratchI)
    ca.Sqrt_I(I3)
    I3 += eps
    ca.Div(I_out, Ibar, I3)


def VarianceEqualize_I(Im, sigma=5.0, eps=1e-3):
    '''Takes an Image and gives the variance equalized version.

    sigma is the width (in voxels) of the gaussian kernel
    eps is the regularizer'''
    VarianceEqualize(Im, Im, sigma, eps)


def ApplyAffine(Iout, Im, A, origin=None, bg=ca.BACKGROUND_STRATEGY_PARTIAL_ZERO):
    '''Applies an Affine matrix A to an image Im.  Also works if Im is
    a Field3D.

    Default for the origin is the center of the image'''

    # assume we're doing center affine w/ index coordinates
    if origin is None:
        print "Warning: no origin given. using image center"
        oldgrid_out = Iout.grid()
        oldgrid_in = Im.grid()

        newgrid_out = cc.MakeGrid(oldgrid_out.size(), [1, 1, 1], 'center')
        newgrid_in = cc.MakeGrid(oldgrid_in.size(), [1, 1, 1], 'center')

        Im.setGrid(newgrid_in)
        Iout.setGrid(newgrid_out)

        ApplyAffineReal(Iout, Im, A, bg)

        Im.setGrid(oldgrid_in)  # reset to original grids
        Iout.setGrid(oldgrid_out)

        return

    print "Warning: this method is slow. ApplyAffineReal is better"

    ca.SetMem(Iout, 0.0)
    h = ca.Field3D(Im.grid(), Im.memType())
    ca.SetToIdentity(h)
    A = np.matrix(A)

    if A.shape[1] == 3:          # 2D affine matrix
        x = ca.Image3D(Im.grid(), Im.memType())
        y = ca.Image3D(Im.grid(), Im.memType())
        xnew = ca.Image3D(Im.grid(), Im.memType())
        ynew = ca.Image3D(Im.grid(), Im.memType())
        ca.Copy(x, h, 0)
        ca.Copy(y, h, 1)
        ca.SetToZero(h)
        # scratchI = ca.Image3D(Im.grid(), Im.memType())
        if origin is None:
            origin = [(Im.grid().size().x+1)/2.0, (Im.grid().size().y+1)/2.0]

        x -= origin[0]
        y -= origin[1]

        # # Matrix Multiply - this is only if A is an inverse affine
        # ca.MulC_Add_MulC(xnew, x, A[0, 0], y, A[0, 1])
        # xnew += (A[0,2]+origin[0])
        # ca.MulC_Add_MulC(ynew, x, A[1, 0], y, A[1, 1])
        # ynew += (A[1,2]+origin[1])

        # Matrix Multiply

        Ainv = A.I
        ca.MulC_Add_MulC(xnew, x, Ainv[0, 0], y, Ainv[0, 1])
        xnew += (Ainv[0, 2]+origin[0])
        ca.MulC_Add_MulC(ynew, x, Ainv[1, 0], y, Ainv[1, 1])
        ynew += (Ainv[1, 2]+origin[1])

        ca.Copy(h, xnew, 0)
        ca.Copy(h, ynew, 1)

        # import PyCACalebExtras.Display as cd
        # print A

        ca.ApplyH(Iout, Im, h, bg)

        # cd.DispVMag(Iout)
        # cd.DispVMag(Im)
        # import PyCACalebExtras.Display
        # cd.DispHGrid(h)
        # sys.exit()
    elif A.shape[1] == 4:         # 3D affine matrix
        x = ca.Image3D(Im.grid(), Im.memType())
        y = ca.Image3D(Im.grid(), Im.memType())
        z = ca.Image3D(Im.grid(), Im.memType())
        xnew = ca.Image3D(Im.grid(), Im.memType())
        ynew = ca.Image3D(Im.grid(), Im.memType())
        znew = ca.Image3D(Im.grid(), Im.memType())
        ca.Copy(x, h, 0)
        ca.Copy(y, h, 1)
        ca.Copy(z, h, 2)

        # import PyCACalebExtras.Display as cd
        # cd.DispImage(x, title='x', cmap='jet', colorbar=True)
        # cd.DispImage(y, title='y', cmap='jet', colorbar=True)
        # cd.DispImage(z, title='z', cmap='jet', colorbar=True)

        if origin is None:
            origin = [(Im.grid().size().x+1)/2.0,
                      (Im.grid().size().y+1)/2.0,
                      (Im.grid().size().z+1)/2.0]

        x -= origin[0]
        y -= origin[1]
        z -= origin[2]

        Ainv = A.I
        ca.MulC_Add_MulC(xnew, x, Ainv[0, 0], y, Ainv[0, 1])
        ca.Add_MulC_I(xnew, z, Ainv[0, 2])
        xnew += (Ainv[0, 3] + origin[0])
        ca.MulC_Add_MulC(ynew, x, Ainv[1, 0], y, Ainv[1, 1])
        ca.Add_MulC_I(ynew, z, Ainv[1, 2])
        ynew += (Ainv[1, 3] + origin[1])
        ca.MulC_Add_MulC(znew, x, Ainv[2, 0], y, Ainv[2, 1])
        ca.Add_MulC_I(znew, z, Ainv[2, 2])
        znew += (Ainv[2, 3] + origin[2])

        ca.Copy(h, xnew, 0)
        ca.Copy(h, ynew, 1)
        ca.Copy(h, znew, 2)

        ca.ApplyH(Iout, Im, h, bg)


def AtoH(h, A):
    '''given A (forward), produce the equivalent H-field'''

    print 'WARNING: AtoH is depreciated (and uses way too much memory). ' + \
        'Please use AtoHIndex (or possibly AtoHReal)'

    ca.SetToIdentity(h)
    grid = h.grid()
    mType = h.memType()
    A = np.matrix(A)

    if A.shape[1] == 3:          # 2D affine matrix
        x = ca.Image3D(grid, mType)
        y = ca.Image3D(grid, mType)
        xnew = ca.Image3D(grid, mType)
        ynew = ca.Image3D(grid, mType)
        ca.Copy(x, h, 0)
        ca.Copy(y, h, 1)
        ca.SetToZero(h)
        # scratchI = ca.Image3D(grid, mType)

        origin = grid.origin().tolist()
        spacing = grid.spacing().tolist()

        # convert to World Coords
        x *= spacing[0]
        y *= spacing[1]
        x += origin[0]
        y += origin[1]

        # Matrix Multiply (inverse, in world coords)
        Ainv = A.I
        ca.MulC_Add_MulC(xnew, x, Ainv[0, 0], y, Ainv[0, 1])
        xnew += (Ainv[0, 2])
        ca.MulC_Add_MulC(ynew, x, Ainv[1, 0], y, Ainv[1, 1])
        ynew += (Ainv[1, 2])

        # convert back to Index Coords
        xnew -= origin[0]
        ynew -= origin[1]
        xnew /= spacing[0]
        ynew /= spacing[1]

        ca.Copy(h, xnew, 0)
        ca.Copy(h, ynew, 1)

    elif A.shape[1] == 4:         # 3D affine matrix
        x = ca.Image3D(grid, mType)
        y = ca.Image3D(grid, mType)
        z = ca.Image3D(grid, mType)
        xnew = ca.Image3D(grid, mType)
        ynew = ca.Image3D(grid, mType)
        znew = ca.Image3D(grid, mType)
        ca.Copy(x, h, 0)
        ca.Copy(y, h, 1)
        ca.Copy(z, h, 2)

        origin = grid.origin().tolist()
        spacing = grid.spacing().tolist()

        # convert to World Coords
        x *= spacing[0]
        y *= spacing[1]
        z *= spacing[2]
        x += origin[0]
        y += origin[1]
        z += origin[2]

        # Matrix Multiply (inverse, in World Coords)
        Ainv = A.I
        ca.MulC_Add_MulC(xnew, x, Ainv[0, 0], y, Ainv[0, 1])
        ca.Add_MulC_I(xnew, z, Ainv[0, 2])
        xnew += (Ainv[0, 3])
        ca.MulC_Add_MulC(ynew, x, Ainv[1, 0], y, Ainv[1, 1])
        ca.Add_MulC_I(ynew, z, Ainv[1, 2])
        ynew += (Ainv[1, 3])
        ca.MulC_Add_MulC(znew, x, Ainv[2, 0], y, Ainv[2, 1])
        ca.Add_MulC_I(znew, z, Ainv[2, 2])
        znew += (Ainv[2, 3])

        # convert back to Index Coords
        xnew -= origin[0]
        ynew -= origin[1]
        znew -= origin[2]
        xnew /= spacing[0]
        ynew /= spacing[1]
        znew /= spacing[2]

        ca.Copy(h, xnew, 0)
        ca.Copy(h, ynew, 1)
        ca.Copy(h, znew, 2)


def AtoHReal(h, A):
    '''given A, returns h (in real coords) such that h = A*x
    '''
    cc.SetToRealIdentity(h)
    cc.MatrixMul_I(A[0:-1,0:-1], h)
    t = np.zeros(3)
    t[0:len(A)-1] = np.squeeze(A[0:-1, -1])
    ca.AddC_I(h, ca.Vec3Df(t[0], t[1], t[2]))


def AtoHIndex(h, A):
    '''given A (defined as a affine transformation in real coords, i.e.
    h(real) = A*x(real), returns h (in index coords) that corresponds to h(real)
    '''
    AtoHReal(h, A)
    cc.HtoIndex(h)


def ApplyAffineReal(Iout, Im, A, bg=ca.BACKGROUND_STRATEGY_PARTIAL_ZERO):
    '''Applies a forward Affine matrix A to an image Im.  Also works if Im is
    a Field3D.

    Uses the ITK origin and spacing to determine the transformation
    the origin is'''
    ca.SetMem(Iout, 0.0)

    h = ca.Field3D(Iout.grid(), Iout.memType())
    AtoHReal(h, np.linalg.inv(A))

    ca.SubC_I(h, Im.grid().origin())
    ca.DivC_I(h, Im.grid().spacing())

    ca.ApplyH(Iout, Im, h, bg)


def DilateMask(mask):
    '''makes the binary mask one pixel bigger around boundaries
    (along 4/6 connected components)'''
    tmp = mask.copy()
    cc.Laplacian(tmp, mask)
    ca.Add_MulC_I(tmp, mask, 5.0)  # add back cental term
    from . import Logic
    Logic.SetRegionGT(mask, tmp, 0.0, 1.0)


def ErodeMask(mask):
    '''makes the binary mask one pixel smaller around boundaries
    (along 4/6 connected components)'''
    tmp = mask.copy()
    cc.Laplacian(tmp, mask)
    ca.Add_MulC_I(tmp, mask, 5.0)  # add back cental term
    from . import Logic as Logic
    Logic.SetRegionLT(mask, tmp, 5.0, 0.0)


def BlendBackground(im, mask, sigma=2.0, numIters=10):
    '''successively applies a gaussian filter to the image background
     (hence softening the boundaries of the image).
    Any part of the image where mask==1 is preserved'''
    # set background to average value
    im *= mask                  # just in case
    scratchI = im.copy()
    ifilt = im.copy()

    from . import Logic as Logic
    bgval = ca.Sum(im)/ca.Sum(mask)  # set average value to background
    Logic.SetRegionEQ(im, mask, 0, bgval)

    g = SetGaussianFilter(im, sigma)
    for _ in xrange(numIters):
        g.filter(ifilt, im, scratchI)  # filter im
        Logic.SetRegionEQ(im, mask, 0, ifilt)  # update im
