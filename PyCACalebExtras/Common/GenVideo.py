''' GenVideo has two functions: MakeVideo(prefix, imList) and
MakeGrid(prefix, imlist) that creates movies/grids in the folder
'prefix' based on a stack of images/V fields, respectively
'''
import os
import shutil
import sys

import PyCA.Core as ca

from subprocess import call
from PyCACalebExtras.Common import Is3D, OffDim, MinMaxList, WritePNG, GetAspect, SubVol


def MakeVideo(prefix, imList, name='movie', fps=5,
              axis='default', imWin=None, dim='z', sliceIdx=None):
    """Make a video from a list of Image3Ds

    Parameters:
    ----------------
    prefix (string) : (relative) folder location to place the movie (images will be placed in
                prefix/videofiles/moviename/ )
    imList (list) : list of 2D PyCA Image3Ds to be turned into a movie
    name : (string, optional) name of the movie file (and folder containing the image frames).
      Default is 'movie'
    fps (int, optional)) : frames per second; default is 5

    axis (string, optional) : Defines the coordinate system
      used. Current options are 'default' and 'cart'.
    'default' has (0,0) in the upper left hand corner and the x
      direction is vertical.
    'cart' has (0,0) in the lower left hand corner and
      the x direction is horizontal.

    imwin ([float, float], optional). : The intensity window.  If
      'None', the window is the [min(imList), max(imList)]
    """
    import PyCA.Common as common

    im = imList[0]

    if Is3D(im):
        if isinstance(im, ca.Image3D):
            # im = common.ExtractSliceIm(im, dim=dim, sliceIdx=sliceIdx)
            imList = [common.ExtractSliceIm(imm, dim=dim, sliceIdx=sliceIdx)
                      for imm in imList]
        else:
            # im = common.ExtractSliceVF(im, dim=dim, sliceIdx=sliceIdx)
            imList = [common.ExtractSliceVF(imm, dim=dim, sliceIdx=sliceIdx)
                      for imm in imList]
    else:
        dim = OffDim(im)

    assert isinstance(prefix, str)

    if prefix[-1] != '/':
        prefix += '/'
    imfolder = prefix + 'video_files/' + name + '/'  # folder for frames

    # remove previous video_files directory and recreate
    try:
        shutil.rmtree(imfolder)
    except OSError:
        pass
    os.makedirs(imfolder)

    # Write PNGs
    if imWin is None:
        imWin = MinMaxList(imList)
    assert imWin[1] > imWin[0]
    for i, im in enumerate(imList):
        # rescale
        if isinstance(im, ca.Image3D):
            rescaled = ca.Image3D(im.grid(), im.memType())
        else:
            rescaled = ca.Field3D(im.grid(), im.memType())  # for color movies
        ca.SubC(rescaled, im, imWin[0])
        ca.DivC_I(rescaled, imWin[1] - imWin[0])
        # # truncate
        # ca.MinC_I(rescaled, 1.0)
        # ca.MaxC_I(rescaled, 0.0)
        # write
        fname = 'im' + str(i).zfill(len(str(len(imList)))) + '.png'

        # if this image has odd dimensions (bad for ffmpeg), crop by 1 pixel
        # this probably doesn't effect ratio too much...
        if dim != 'x' and rescaled.size().x % 2 == 1:
            rescaled = SubVol(rescaled, xrng=[0, rescaled.size().x - 1])
        if dim != 'y' and rescaled.size().y % 2 == 1:
            rescaled = SubVol(rescaled, yrng=[0, rescaled.size().y - 1])
        if dim != 'z' and rescaled.size().z % 2 == 1:
            rescaled = SubVol(rescaled, zrng=[0, rescaled.size().z - 1])

        WritePNG(rescaled, imfolder + fname, axis)

    scale = GetAspect(rescaled.grid(), dim, axis, False)
    MakeVideoFromFiles(prefix + name, imfolder, fps, scale)
    # MakeVideoFromFiles(prefix + name, imfolder, fps)


def MakeDefGridVideo(prefix, uList, name='grid', fps=5, splat=False, dim='z'):
    '''takes Field3D list, and makes pngs/videos of deformation
    grids'''

    import matplotlib.pyplot as plt
    import pylab                # not sure i actually need this
    import PyCACalebExtras.Display as cd
    plt.switch_backend('Agg')

    if prefix[-1] != '/':
        prefix += '/'
    imfolder = prefix + 'grid_video_files/' + name + '/'  # folder for frames

    # remove previous video_files directory and recreate
    try:
        shutil.rmtree(imfolder)
    except OSError:
        pass
    os.makedirs(imfolder)

    # write images
    for i, u in enumerate(uList):
        plt.close('all')
        u = uList[i]
        fname = name + str(i).zfill(len(str(len(uList)))) + '.png'
        cd.DispVGrid(u, splat=splat, dim=dim)
        # pylab.savefig(prefix + '/movies/tmp/' + fname,
        #                   bbox_inches='tight', pad_inches=0)
        pylab.savefig(imfolder + fname,
                      bbox_inches='tight', pad_inches=0)
        plt.close('all')

    MakeVideoFromFiles(prefix + name, imfolder, fps)


def MakeVideoFromFiles(videoName, fileDir, fps, scale=None):
    '''makes a video assuming the pngs for that video are already
    created'''
    devnull = open('/dev/null', 'w')

    cmd1 = "mencoder \"mf://" + fileDir + "*.png\" -mf fps=" + str(fps) + \
        " -o " + videoName + ".avi" + \
        " -ovc lavc -lavcopts vcodec=mpeg4 -oac copy"
    # print cmd1

    # Expand Video if necessary
    if scale is not None:
        scalex, scaley = scale  # pylint: disable=W0633
        scale = "-vf scale={0}:{1} ".format(scalex, scaley)
        scale += " -aspect {0}:{1}".format(scalex, scaley)
    else:
        scale = ""

    # print scale

    cmd2 = ["ffmpeg -r {}".format(fps),
            "-pattern_type glob -i \'{}/*.png\'".format(fileDir),
            "-y",
            "-c:v libx264 -pix_fmt yuv420p",
            "-crf 18",
            scale,
            videoName + ".mp4"]
    cmd2 = " ".join(cmd2)
    # print cmd2

    # try ffmpeg
    try:
        returncode = call(cmd2, shell=True, stdout=devnull, stderr=devnull)
        if returncode == 127:
            print "ffmpeg command not found"
        elif returncode != 0:
            print "ffmpeg - some other error; return code =", returncode
            print "tried calling"
            print cmd2
        else:
            print 'Wrote video ' + videoName + '.mp4'
            return
    except OSError as e:
        print "ffmpeg Fails"
        print "OS Error({0}): {1}".format(e.errno, e.strerror)
        print cmd2
    except:                     # pylint: disable=W0702
        print "ffmpeg Fails"
        print "Unexpected error:", sys.exc_info()[0]
        print cmd2

    # try mencoder
    try:
        returncode = call(cmd1, shell=True, stdout=devnull, stderr=devnull)
        if returncode == 127:
            print "mencoder command not found"
        elif returncode != 0:
            print "mencoder - some other error; return code =", returncode
        else:
            print 'Wrote video ' + videoName + '.avi'
            return
    except OSError as e:
        print "Mencoder Fails"
        print "OS Error({0}): {1}".format(e.errno, e.strerror)
        print cmd1
        print "trying ffmpeg"
    except:                     # pylint: disable=W0702
        print "Mencoder Fails"
        print "Unexpected error:", sys.exc_info()[0]
        print cmd1
        print "trying ffmpeg"
        # os.system(cmd)

    print 'Both Mencoder and ffmpeg fails'
    print 'Try exiting iPython and running:'
    print 'cc.MakeVideoFromFiles(\"' + videoName + '\",\"' + \
        fileDir + '\", ' + str(fps) + ')'
