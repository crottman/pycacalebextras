'''File I/O for PyCA'''
import glob
import PyCA.Core as ca
from . import nrrd
import numpy as np
import sys
from . import mha
from PyCACalebExtras.Common import Is3D, Imlist_to_Im, MakeGrid, FlipAxis, SwapAxes
import os
import json
import PyCA.Common as common


class PyCACalebExtrasException(Exception):
    '''Exception class for PyCACalebExtras'''
    pass


# following the example from common.LoadITKImage and
# common.WriteITKImage, a load data function is always
# LoadData(filename, mType) and a write data function is
# WriteData(im, filename)

def LoadPNG(filename, mType=ca.MEM_DEVICE, ds=1, col='bw'):
    '''loads a png and returns it as an Image3D

    ds=4 downsamples an image by 4 (1024x1024 => 256x256)

    col='bw' loads a black and white Image3D
    col='rgb' loads a RGB Field3D
    '''
    import matplotlib.pyplot as plt

    ImArr = plt.imread(filename)
    if col == 'bw':
        if len(ImArr.shape) > 2:
            if ImArr.shape[2] == 3:
                ImArr = ImArr.mean(2)
            elif ImArr.shape[2] == 4:
                ImArr = ImArr[:, :, 0:3].mean(2)

        # ImArr = ImArr[::ds, ::ds]

        ImArr = sum([ImArr[i::ds, j::ds]
                     for i in range(ds) for j in range(ds)]) # downsample
        ImArr *= 1/(ds**2*1.0)
        Im = common.ImFromNPArr(ImArr, mType)
        return Im
    elif col == 'rgb':
        if len(ImArr.shape) != 3:
            raise PyCACalebExtrasException("Not a color image!")
        # downsample each channel and then concat
        ImArr = np.concatenate(
            (np.atleast_3d(sum([ImArr[i::ds, j::ds, 0]
                                for i in range(ds) for j in range(ds)])),
             np.atleast_3d(sum([ImArr[i::ds, j::ds, 1]
                                for i in range(ds) for j in range(ds)])),
             np.atleast_3d(sum([ImArr[i::ds, j::ds, 2]
                                for i in range(ds) for j in range(ds)]))),
            axis=2)
        ImArr *= 1/(ds**2*1.0)
        ImArr = ImArr.reshape(ImArr.shape[0], ImArr.shape[1], 1, ImArr.shape[2])
        Fi = common.FieldFromNPArr(ImArr, mType)
        return Fi
    else:
        raise NotImplementedError("unknown color '" + col + "'")


def WritePNG(Im, filename, axis='default', rng=None, dim='z', sliceIdx=None):
    '''writes a bw/color png given an Image3D/Field3D

    the default range is (0-1), but can be given as 'imrange' (same as
    DispImage), or can be specified as a list of two numbers,
    i.e. rng=[0, 255]

    axis is the axis direction.  'default' has (0,0) in the upper left hand corner
                                      and the x direction is vertical
                                 'cart' has (0,0) in the lower left hand corner
                                      and the x direction is horizontal
    '''
    WriteTIFF(Im, filename, axis, rng, dim, sliceIdx)     # works the same


def LoadTIFF(filename, mType=ca.MEM_DEVICE, ds=1):
    '''loads a black and white tiff and returns it as an Image3D

    If a color image is read, a black and white image is returned
    (averaging the channels)

    You can set the downsampling rate. For example, downsample an
    image by ds=4 (1024x1024 => 256x256)

    '''
    import matplotlib.pyplot as plt

    ImArr = plt.imread(filename)
    ImArr = np.float32(ImArr)
    if len(ImArr.shape) > 2:
        if ImArr.shape[2] == 3:
            ImArr = ImArr.mean(2)
        elif ImArr.shape[2] == 4:
            ImArr = ImArr[:, :, 0:3].mean(2)
        else:
            raise PyCACalebExtrasException('Unknown array size')
    # ImArr = ImArr[::ds, ::ds]   # subsample
    ImArr = sum([ImArr[i::ds, j::ds]
                 for i in range(ds) for j in range(ds)]) # downsample
    ImArr *= 1/(ds**2*1.0)
    Im = common.ImFromNPArr(ImArr, mType)
    Im /= 255.0                   # convert to 0-1 range
    return Im


def WriteTIFF(Im, filename, axis='default', rng=None, dim='z', sliceIdx=None):
    '''writes a bw/color tiff given an Image3D/Field3D

    the default range is (0, 1), but can be given as 'imrange' (same as
    DispImage), or can be specified as a list of two numbers,
    i.e. rng=[0, 255]

    axis is the axis direction.  'default' has (0,0) in the upper left hand corner
                                      and the x direction is vertical
                                 'cart' has (0,0) in the lower left hand corner
                                      and the x direction is horizontal
    '''
    import matplotlib.pyplot as plt

    if Is3D(Im):
        Im = common.ExtractSliceIm(Im, dim=dim, sliceIdx=sliceIdx)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    # import scipy.misc
    ImArr = np.squeeze(Im.asnp())
    if axis == 'cart':
        ImArr = np.flipud(ImArr.T)
    if rng is None:
        vmin, vmax = 0, 1
    elif rng == 'imrange':
        [vmin, vmax] = ca.MinMax(Im)
        if vmax - vmin == 0:
            vmin -= 1
            vmax += 1
    else:
        vmin, vmax = rng

    # shp = ImArr.shape
    if len(ImArr.shape) > 2:    # color image
        # this is necessary :(
        ImArr[ImArr > vmax] = vmax
        plt.imsave(filename, ImArr.transpose((1, 2, 0)), vmin=vmin, vmax=vmax)
        # import PyCACalebExtras.Display as cd
        # cd.DispImage(Im)
        # sys.exit()
    else:
        plt.imsave(filename, ImArr, cmap='gray', vmin=vmin, vmax=vmax)

    Im.toType(oldType)


def LoadNRRD(filename, mType=ca.MEM_DEVICE, verbose=True):
    '''loads a nrrd and returns it as an Image3D
    '''

    if verbose:
        sys.stdout.write('Loading ' + filename + '...')
        sys.stdout.flush()
    ImArr, options = nrrd.read(filename)

    Im = common.ImFromNPArr(ImArr, mType)
    if 'space origin' in options.keys():
        origin = ca.Vec3Df()
        originlist = options['space origin']
        if len(originlist) == 2:
            originlist.append(0)
        origin.fromlist(originlist)
    else:
        origin = ca.Vec3Df(0, 0, 0)
    if 'spacings' in options.keys():
        spacinglist = options['spacings']
        if len(spacinglist) == 2:
            spacinglist.append(1)
        spacing = ca.Vec3Df()
        spacing.fromlist(spacinglist)
    elif 'space directions' in options.keys():
        spx = options['space directions'][0][0]
        spy = options['space directions'][1][1]
        if Im.size().z > 1:
            spz = options['space directions'][2][2]
        else:
            spz = 1.0
        spacing = ca.Vec3Df(spx, spy, spz)
    else:
        spacing = ca.Vec3Df(1, 1, 1)
    grid = ca.GridInfo(Im.grid().size(),
                       spacing,
                       origin)

    Im.setGrid(grid)
    if verbose:
        sys.stdout.write('done\n')
    return Im


def WriteNRRD(im, filename):
    '''Writes an Image3D as an NRRD file
    '''
    sys.stdout.write('Saving ' + filename + '...')
    sys.stdout.flush()
    oldType = im.memType()
    im.toType(ca.MEM_HOST)
    ImArr = im.asnp()
    options = {'spacings': im.grid().spacing().tolist(),
               'space origin': im.grid().origin().tolist()}
    nrrd.write(filename, ImArr, options)
    im.toType(oldType)
    sys.stdout.write('done\n')


def LoadNII(filename, mType=ca.MEM_DEVICE):
    '''loads a nifti file: requires NiBabel

    this is very minimally tested
    '''
    import nibabel as nib
    img = nib.load(filename)
    sys.stdout.write('Loading ' + filename + '...')
    sys.stdout.flush()

    grid = ca.GridInfo(ca.Vec3Di(*img.header['dim'].astype(int)[1:4]))
    grid.setSpacing(ca.Vec3Df(*img.header['pixdim'].astype(float)[1:4]))
    grid.setOrigin(ca.Vec3Df(-float(img.header['qoffset_x']),
                             -float(img.header['qoffset_y']),
                             float(img.header['qoffset_z'])))

    if img.header['dim'][5] == 3:
        ob = common.FieldFromNPArr(np.squeeze(img.get_data()), mType)
    else:
        ob = common.ImFromNPArr(np.squeeze(img.get_data()), mType)
    ob.setGrid(grid)

    sys.stdout.write('done\n')
    return ob


def WriteNII(im, filename):
    '''write nifti file'''
    raise NotImplementedError


def WriteRAW(im, filename, append=False, dtype=None):
    '''Writes an Image3D as a raw file'''
    if not isinstance(im, ca.Image3D):
        print 'WriteRAW only works for Image3Ds'
        sys.exit()
    if not append:
        sys.stdout.write('Saving ' + filename + '...')
        sys.stdout.flush()
    oldType = im.memType()
    im.toType(ca.MEM_HOST)
    if dtype is not None:
        ImArr = im.asnp().astype(dtype).copy()
    else:
        ImArr = im.asnp().copy()
    if append:
        openas = 'ab'
    else:
        openas = 'wb'
    with open(filename, openas) as f:
        f.write(ImArr[:, :, :])

    im.toType(oldType)
    if not append:
        sys.stdout.write('done\n')


def WriteNRRDHeader(grid, filename, dtype):
    '''writes a nrrd header (.nhdr): assumes that there is an data
    *.raw that is written'''
    with open(filename, 'wb') as f:
        f.write("NRRD0004\n")
        if dtype == np.uint8:
            f.write("type: UCHAR\n")
        else:
            print "Unknown dtype! exiting"
            return
        f.write("dimension: 3\n")
        f.write("sizes: {0} {1} {2}\n".format(grid.size().x, grid.size().y, grid.size().z))
        f.write("spacings: {0} {1} {2}\n".format(grid.spacing().x,
                                                 grid.spacing().y,
                                                 grid.spacing().z))
        # f.write("space origin: ({0},{1},{2})\n".format(grid.origin().x,
        #                                                grid.origin().y,
        #                                                grid.origin().z))
        f.write("endian: little\n")
        f.write("encoding: raw\n")
        nrrdfname = os.path.basename(filename)[:-4]
        f.write("data file: ./{}raw\n".format(nrrdfname))


def LoadMHA(filename, mType=ca.MEM_DEVICE, verbose=True):
    '''Loads a grayscale MHA file to an Image3D'''

    # check to see if its a field or a image
    if filename[-4:] != '.mha' and filename[-4:] != '.mhd':
        filename += '.mha'
    if verbose:
        sys.stdout.write('Loading ' + filename + '...')
        sys.stdout.flush()

    head = []
    with open(filename) as f:
        for _ in xrange(14):
            try:
                head += [next(f)]
            except StopIteration:  # in case shorter than 14 (mhd, for example)
                break
    if "ElementNumberOfChannels = 3\n" in head:
        im = common.LoadITKField(filename, mType)
    elif "ElementNumberOfChannels = 4\n" in head:
        print "warning, loading an RGBA image as a Field3D"
        im = common.LoadITKField(filename, mType)
    else:
        im = common.LoadITKImage(filename, mType)
    if verbose:
        sys.stdout.write('done\n')
    return im


def WriteMHA(im, filename, useCompression=False, verbose=True):
    """Write an Image3D as an MHA file.

    Parameters:
    -----------
    im (Image3D or list) : If the input is a list of 2D
      Image3Ds, then the a single 3D MHA is written, appending the
      Image3Ds along the 'z' direction (where the grid/spacing is
      defined by the first image in the list
    filename (string) : filename of MHA file to write. If '.mha' or '.mhd'
      doesn't end the file, '.mha' will automatically be added
    """

    if verbose:
        sys.stdout.write('Saving ' + filename + '...')
        sys.stdout.flush()

    if filename[-4:] != '.mha' and filename[-4:] != '.mhd':
        filename += '.mha'

    if isinstance(im, ca.Image3D):
        common.SaveITKImage(im, filename, useCompression)
        if verbose:
            sys.stdout.write('done\n')
    elif isinstance(im, ca.Field3D):
        common.SaveITKField(im, filename, useCompression)
        if verbose:
            sys.stdout.write('done\n')
    else:                       # list of *3Ds
        print "WARNING - UNTESTED, maybe just write the full block?"
        sys.stdout.write('Saving ' + filename + '...')
        sys.stdout.flush()
        iml = im
        if not all((iml[0].memType() == im.memType() for im in iml)):
            raise Exception('All Images in list must have the same memType')
        if not all((iml[0].grid().size().tolist() == im.grid().size().tolist()
                    for im in iml)):
            raise Exception('All Images in list must be the same size')
        oldType = iml[0].memType()
        for im in iml:
            im.toType(ca.MEM_HOST)

        Imnp = np.concatenate([im.asnp().T for im in iml], axis=0)
        sz = iml[0].grid().size().tolist()
        sz[2] = len(iml)

        spacing = iml[0].grid().spacing().tolist()
        origin = iml[0].grid().origin().tolist()

        # Imnp *= 255
        # Imnp = Imnp.astype(np.uint8)

        mhabw = mha.new(data=Imnp, size=sz, spacing=spacing, offset=origin)

        mhabw.write_mha(filename)
        for im in iml:
            im.toType(oldType)
        if verbose:
            sys.stdout.write('done\n')


def LoadColorMHA(filename, mType=ca.MEM_DEVICE):
    '''Loads a color MHA file to an Field3D where the x, y, z
    components of the Field3D are r, g, b. (a is discarded)

    This loads an rgba (4 channel) image. If it is a vector field,
    load to a Field3D using LoadMHA
    '''
    # return common.LoadITKField(filename, mType)

    sys.stdout.write('Loading rgba ' + filename + '...')
    sys.stdout.flush()
    img = mha.new(input_file=filename)
    grid = ca.GridInfo(ca.Vec3Di(img.size[0], img.size[1], img.size[2]),
                       ca.Vec3Df(img.spacing[0], img.spacing[1], img.spacing[2]),
                       ca.Vec3Df(img.offset[0], img.offset[1], img.offset[2]))

    if img.data.shape[3] != 4:
        raise PyCACalebExtrasException("Not an rgba image. Use LoadMHA" +
                                       "if you are loading a scalar image or field")

    Ir = common.ImFromNPArr(img.data[:, :, :, 0], ca.MEM_HOST)
    Ir.setGrid(grid)
    Ig = common.ImFromNPArr(img.data[:, :, :, 1], ca.MEM_HOST)
    Ig.setGrid(grid)
    Ib = common.ImFromNPArr(img.data[:, :, :, 2], ca.MEM_HOST)
    Ib.setGrid(grid)

    Icol = ca.Field3D(grid, ca.MEM_HOST)

    ca.Copy(Icol, Ir, 0)
    ca.Copy(Icol, Ig, 1)
    ca.Copy(Icol, Ib, 2)

    print ca.MinMax(Ir)
    print ca.MinMax(Ig)
    print ca.MinMax(Ib)
    print ca.MinMax(Icol)

    Icol.setGrid(grid)
    Icol.toType(mType)

    Icol /= 255.0

    sys.stdout.write('done\n')
    return Icol


def WriteColorMHA(G, filename):
    '''Writes a color MHA given a Field3D - such that the x, y, and z
    components of the Field3D are r, g, and b'''

    sys.stdout.write('Saving rgba ' + filename + '...')
    sys.stdout.flush()

    if filename[-4:] != '.mha' and filename[-4:] != '.mhd':
        filename += '.mha'

    if isinstance(G, list):
        if not isinstance(G[0], ca.Field3D):
            raise PyCACalebExtrasException("Not a list of Field3Ds")
        sys.stdout.write(' (converting to Field3Ds...')
        sys.stdout.flush()
        G = Imlist_to_Im(G)
        sys.stdout.write('done)')
        sys.stdout.flush()

    if not isinstance(G, ca.Field3D):
        raise PyCACalebExtrasException("Not a Field3D!")

    oldType = G.memType()
    G.toType(ca.MEM_HOST)

    c = 1.0

    mm = ca.MinMax(G)
    if mm[0] < -.49:
        print "Warning, this field has negative values! (wraparound)"

    # set to viewable range
    if 128 < mm[1] <= 255:
        c = 1.0                 # don't scale
    elif 0.5 < mm[1] <= 1.0:
        c = 255.0
    else:
        c = 255.0/mm[1]         # scale to max value

    sz = G.grid().size().tolist()

    Gnpx = c*G.x_asnp().copy()           # .copy() is definitely necessary
    Gnpy = c*G.y_asnp().copy()
    Gnpz = c*G.z_asnp().copy()
    Gnpa = (Gnpx + Gnpy + Gnpz)/3.0  # alpha channel
    Gnpl = [Gnpx[..., None], Gnpy[..., None], Gnpz[..., None], Gnpa[..., None]]

    # 2D tests
    # fakesize = [sz[0], sz[1], 4, sz[2]]  # ijlk
    # fakesize = [sz[0], 4, sz[1], sz[2]]  # iljk, gray
    # fakesize = [sz[1], sz[0], 4, sz[2]]  # jilk, works for 2D
    # fakesize = [sz[1], 4, sz[0], sz[2]]  # jlik, 3gray vert
    # fakesize = [4, sz[0], sz[1], sz[2]]  # lijk, 3 gray horiz
    # fakesize = [4, sz[1], sz[0], sz[2]]  # ljik, 9 gray

    # 3D tests
    # fakesize = [sz[1], sz[0], 4, sz[2]]  # jilk, YES
    # fakesize = [sz[1], sz[0], sz[2], 4]  # jikl, no
    # fakesize = [sz[1], sz[2], sz[0], 4]  # jkil, no
    # fakesize = [sz[2], sz[1], sz[0], 4]  # kjil, no

    # # This is one way to do it
    # fakesize = [sz[1], sz[0], 4, sz[2]]
    # Gnp = np.zeros(fakesize, dtype = np.uint8)
    # for i in xrange(sz[0]):
    #     for j in xrange(sz[1]):
    #         for k in xrange(sz[2]):
    #             for l in xrange(4):
    #                 Gnp[j,i,l,k] = Gnpl[l][i,j,k]

    Gnp = np.concatenate(Gnpl, 3)  # last dim
    # swap around the axes orders to get it right
    Gnp = np.swapaxes(Gnp, 0, 1)
    Gnp = np.swapaxes(Gnp, 2, 3)

    # using mha module
    mhacol = mha.new(data=Gnp.astype(np.uint8), size=sz,
                     spacing=G.grid().spacing().tolist(),
                     offset=G.grid().origin().tolist())
    mhacol.write_mha(filename)

    # convert back to correct memory
    G.toType(oldType)
    sys.stdout.write('done\n')


def LoadDICOM(filenamewc, mType=ca.MEM_DEVICE, slices=None):
    '''loads a stack of dicoms: filenamewc is a wildcard search for
    the stack of dicoms

    can optionally only load the first N slices, or slices [a, b)'''
    import dicom

    sys.stdout.write('Loading DICOM file ' + filenamewc + '...')
    sys.stdout.flush()

    alldicoms = sorted(glob.glob(filenamewc))  # dicom should sortable

    if slices is not None:
        try:                    # slices = [startslice, endslice]
            alldicoms = alldicoms[slices[0]:slices[1]]
        except TypeError:
            alldicoms = alldicoms[0:slices]  # slices = N (first n slices)

    NSlices = len(alldicoms)
    if NSlices == 0:
        raise IOError('No dicom files found')
    elif NSlices == 1:
        print 'Warning: only one dicom slice found'
    else:
        print 'Found {} DICOM slices'.format(len(alldicoms))

    for i, fname in enumerate(alldicoms):
        ds = dicom.read_file(fname)
        # print ds
        # sys.exit()
        if i == 0:
            try:
                origin0 = ds.ImagePositionPatient
                cosines = ds.ImageOrientationPatient
                spacing = [float(ds.PixelSpacing[1]), float(ds.PixelSpacing[0]),
                           float(ds.SliceThickness)]
                # columns represents LSB in dicom
                size = [ds.Columns, ds.Rows, NSlices]
                cosX = cosines[0:3]
                cosY = cosines[3:6]
            except (AttributeError, ValueError) as e:
                print "Improper DICOM file(s)!"
                raise e
                # raise IOError("Could not open DICOM file")

            if ds.BitsAllocated == 16:
                dtype = np.uint16
            elif ds.BitsAllocated == 32:
                dtype = np.uint32
            else:
                raise IOError('unexpected DICOM datatype')
            arr3D = np.zeros(size)
        if i == NSlices-1:
            origin1 = ds.ImagePositionPatient

        im = np.fromstring(ds.PixelData, dtype=dtype)
        try:
            im = im.reshape(ds.Rows, ds.Columns)
        except ValueError as e:
            print "Unexpected image size!"
            raise
        # let x be LSB
        arr3D[:, :, i] = im.transpose()

    grid = MakeGrid(size, spacing)
    Im = common.ImFromNPArr(arr3D, ca.MEM_HOST)
    Im.setGrid(grid)

    if abs(sum(cosX)) != 1 or abs(sum(cosY)) != 1:
        print "Warning: Not using any cosine info"
        return Im

    # Strategy: assuming cosines only have one direction each,
    # first flip the axes so that they all align with +x, +y, +z
    # then swap axes when necessary

    # flip z direction if necessary:
    if any(oi > oj for oi, oj in zip(origin0, origin1)):
        Im = FlipAxis(Im, 2)
    # flip x direction if necessary:
    if -1 in cosX:
        Im = FlipAxis(Im, 0)
    # flip y direction if necessary:
    if -1 in cosY:
        Im = FlipAxis(Im, 1)

    # flip z axis first if necessary:
    if abs(cosX[2]) == 1:
        Im = SwapAxes(Im, 0, 2)
    elif abs(cosY[2]) == 1:
        Im = SwapAxes(Im, 1, 2)
    # swap x and y axes if necssary
    if abs(cosX[1]) == 1 or abs(cosY[0]) == 1:
        Im = SwapAxes(Im, 0, 1)

    # figure out origin - need to min because of multiple slices
    origin = [min(oi, oj) for oi, oj in zip(origin0, origin1)]  # x-y-z origin orig

    # switch origin direction if necessary
    # this doesn't seem right, but checks out w/ VolView
    if cosX[0] == -1 or cosY[0] == -1:  # flip x-origin
        origin[0] = origin[0]-(size[0])*spacing[0]
    if cosX[1] == -1 or cosY[1] == -1:  # flip y-origin
        origin[1] = origin[1]-(size[1])*spacing[1]
    if cosX[2] == -1 or cosY[2] == -1:  # flip z-origin
        origin[2] = origin[2]-(size[2])*spacing[2]
    Im.setOrigin(ca.Vec3Df(*origin))

    Im.toType(mType)
    sys.stdout.write('done\n')
    return Im


def LoadRAW(fname, grid, dtype=None, mType=ca.MEM_DEVICE):
    '''loads from a raw file. Need a grid and dType specified'''

    sys.stdout.write('Loading Raw file ' + fname + '...')
    sys.stdout.flush()

    fbytes = os.path.getsize(fname)

    isVF = False
    # if no dtype given, try:
    # 1-byte -> np.uint8
    # 2-byte -> np.uint16
    # 4-byte -> np.float32
    # 8-byte -> np.float64
    if dtype is None:
        if grid.nVox()*8*3 < fbytes:  # 24
            print 'float64 vfield'
            dtype = np.float64
            isVF = True
        elif grid.nVox()*4*3 < fbytes:  # 12
            print 'float32 vfield'
            dtype = np.float32
            isVF = True
        elif grid.nVox()*8 < fbytes:  # 8
            print 'float64 image'
            dtype = np.float64
        elif grid.nVox()*4 < fbytes:
            print 'float32 image'
            dtype = np.float32
        elif grid.nVox()*2 < fbytes:
            print 'uint16 image'
            dtype = np.uint16
        else:
            print 'uint8 image'
            dtype = np.uint8

    imbytes = grid.nVox() * dtype().nbytes
    if imbytes * 3 < fbytes:
        isVF = True

    if isVF:
        imbytes *= 3
        raise NotImplementedError('Not loading vector fields yet')

    # calculate header size
    byteoffset = fbytes - imbytes

    if byteoffset < 0:
        print "\nfile size (bytes)", fbytes
        print "im3d size (bytes)", imbytes
        raise IOError("file not big enough for grid")
    elif byteoffset == 0:
        pass
    elif float(byteoffset) / float(fbytes) > .1:
        print "\nfile size (bytes)", fbytes
        print "im3d size (bytes)", imbytes
        print "Warning...file looks like it's too big"
    else:
        pass

    with open(fname, "rb") as f:
        f.seek(byteoffset, os.SEEK_SET)
        arr = np.fromfile(f, dtype=dtype)

    assert arr.size == grid.nVox()

    Im = common.ImFromNPArr(arr, ca.MEM_HOST)

    if fname[-4:] == '.nii':
        print 'Nifty file, reorienting to LPS'
        Im.setSize(ca.Vec3Di(grid.size().y, grid.size().x, grid.size().z))
        Im = SwapAxes(Im, 0, 1)

    Im.setGrid(grid)
    Im.toType(mType)

    sys.stdout.write('done\n')
    return Im


def Load(filename, mType=ca.MEM_DEVICE, col='bw'):
    '''loads a generic file (with extension)'''
    ext = os.path.splitext(filename)[1][1:].lower()  # get extension

    if ext == 'png':
        return LoadPNG(filename, mType, col=col)
    elif ext in ['tiff', 'tif']:
        if col != 'bw':
            raise NotImplementedError('No support for color tiff images')
        return LoadTIFF(filename, mType)
    elif ext in ['mha', 'mhd']:
        if col == 'bw':
            return LoadMHA(filename, mType)
        else:
            return LoadColorMHA(filename, mType)
    elif ext in ['nrd', 'nrrd']:
        return LoadNRRD(filename, mType)
    elif ext in ['dcm']:
        return LoadDICOM(filename, mType)
    elif ext in ['gz', 'nii']:
        return LoadNII(filename, mType)
    else:
        raise NotImplementedError('Unknown File Extension "{}"'.format(ext))


def Write(Im, filename, axis='default', rng=None, dim='z', sliceIdx=None, col='bw'):
    '''writes a generic file (with extension)'''
    ext = os.path.splitext(filename)[1][1:].lower()  # get extension

    if ext == 'png':
        if col != 'bw':
            raise NotImplementedError('No support for writing color png images')
        WritePNG(Im, filename, axis, rng, dim, sliceIdx)
    elif ext in ['tiff', 'tif']:
        if col != 'bw':
            raise NotImplementedError('No support for writing color tiff images')
        WriteTIFF(Im, filename, axis, rng, dim, sliceIdx)
    elif ext in ['mha', 'mhd']:
        if col == 'bw':
            WriteMHA(Im, filename)
        else:
            WriteColorMHA(Im, filename)
    elif ext in ['nrd', 'nrrd']:
        WriteNRRD(Im, filename)
    else:
        raise NotImplementedError('Unknown File Extension "{}"'.format(ext))


def WriteGrid(grid, filename):
    '''saves a pyca gridInfo object using human readable json object'''

    d = {'origin': grid.origin().tolist(),
         'spacing': grid.spacing().tolist(),
         'size': grid.size().tolist()}

    with open(filename, 'w') as handle:
        json.dump(d, handle)


def LoadGrid(filename):
    '''loads a pyca gridInfo object saved using WriteGrid'''

    with open(filename, 'r') as handle:
        d = json.load(handle)

    sz, sp, o = d['size'], d['spacing'], d['origin']

    return ca.GridInfo(ca.Vec3Di(sz[0], sz[1], sz[2]),
                       ca.Vec3Df(sp[0], sp[1], sp[2]),
                       ca.Vec3Df(o[0], o[1], o[2]))
