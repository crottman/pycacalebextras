'''Commonly used functions for PyCA - Caleb Rottman'''
# import sys
# import PyCACalebExtras.Display as cd

import numpy as np

import PyCA.Core as ca

# import matplotlib.pyplot as plt
# plt.ion() # tell it to use interactive mode -- see results immediately


def CreateCircle(Im, R, center=None, val=1.0):
    '''Creates Image3D circle with radius and center
    using index values for x, y'''
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    y, x = np.ogrid[0:Im.size().x, 0:Im.size().y]
    if center is None:
        center = [(Im.size().x-1) / 2.0, (Im.size().y-1) / 2.0]
    Im.asnp()[:, :, :] = val*np.atleast_3d(((x - center[0])**2 +
                                            (y - center[1])**2 < R**2))

    Im.toType(oldType)


def AddCircle(Im, R, center=None, val=1.0):
    '''Adds a circle to Im using CreateCircle'''
    Imcirc = ca.Image3D(Im.grid(), Im.memType())
    CreateCircle(Imcirc, R, center, val)
    Im += Imcirc


def CreatePolygon(Im, points, val=1.0):
    '''Creates Image3D polygon given 3+ pairs of points

    CreatePolygon(Im, [[p0x, p0y], [p1x, p1y], [p2x, p2y], ...], val=1.0) '''
    try:
        from skimage.draw import polygon
    except ImportError:
        print "install package 'python-scikit-image'"
        raise
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    Imnp = Im.asnp()
    x = np.array([pt[0] for pt in points])
    y = np.array([pt[1] for pt in points])
    rr, cc = polygon(y, x)
    Imnp[rr, cc] = val

    Im.toType(oldType)


def AddPolygon(Im, points, val=1.0):
    '''Adds a polygon to Im using CreatePolygon'''
    Impoly = ca.Image3D(Im.grid(), Im.memType())
    CreatePolygon(Impoly, points, val)
    Im += Impoly


def CreateRealCircle(Im, R, center=None, val=1.0):
    '''Creates Image3D circle with radius and center'''
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    x, y = np.ogrid[0:Im.size().x, 0:Im.size().y]
    if center is None:
        center = [Im.size().x / 2.0, Im.size().y / 2.0]
        Im.asnp()[:, :, :] = val*np.atleast_3d(((x - center[0])**2 +
                                                (y - center[1])**2 < R**2))
    else:
        Wx = Im.origin().x + Im.spacing().x*x
        Wy = Im.origin().y + Im.spacing().y*y
        Im.asnp()[:, :, :] = val*np.atleast_3d(((Wx - center[0])**2 +
                                                (Wy - center[1])**2 < R**2))

    Im.toType(oldType)


def AddRealCircle(Im, R, center=None, val=1.0):
    '''Adds a circle to Im using CreateCircle'''
    Imcirc = ca.Image3D(Im.grid(), Im.memType())
    CreateCircle(Imcirc, R, center, val)
    Im += Imcirc


def CreateSphere(Im, R, center=None, val=1.0):
    '''Creates a Sphere in a 3D image with a radius R and uses
    spacing/origin appropriately
    '''
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    x, y, z = np.ogrid[0:Im.size().x, 0:Im.size().y, 0:Im.size().z]
    if center is None:
        center = [Im.size().x / 2.0, Im.size().y / 2.0, Im.size().z / 2.0]
        # center is not in world coords
        Im.asnp()[:, :, :] = val*np.atleast_3d(
            (((x - center[0])*Im.spacing().x)**2 +
             ((y - center[1])*Im.spacing().y)**2 +
             ((z - center[2])*Im.spacing().z)**2 < R**2))
    else:
        # change to world coordinates
        Wx = Im.origin().x + Im.spacing().x*x
        Wy = Im.origin().y + Im.spacing().y*y
        Wz = Im.origin().z + Im.spacing().z*z

        # center is already in world coords
        Im.asnp()[:, :, :] = val*np.atleast_3d((((Wx - center[0]))**2 +
                                                ((Wy - center[1]))**2 +
                                                ((Wz - center[2]))**2 < R**2))

    Im.toType(oldType)


def AddSphere(Im, R, center=None, val=1.0):
    '''Adds a sphere to Im using CreateSphere'''
    Imsphere = ca.Image3D(Im.grid(), Im.memType())
    CreateSphere(Imsphere, R, center, val)
    Im += Imsphere


def CreateRect(Im, corner1, corner2, val=1.0):
    '''Creates Image3D rectangle given two corners'''
    c1 = [min(corner1[0], corner2[0]), min(corner1[1], corner2[1])]
    c2 = [max(corner1[0], corner2[0]), max(corner1[1], corner2[1])]
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    y, x = np.ogrid[0:Im.size().x, 0:Im.size().y]

    # This is python indexing
    Im.asnp()[:, :, :] = val*np.atleast_3d((x >= c1[0]) * (x < c2[0]) *
                                           (y >= c1[1]) * (y < c2[1]))
    Im.toType(oldType)


def AddRect(Im, c1, c2, val=1.0):
    '''Adds a rectangle to Im using CreateRect'''
    Imrect = ca.Image3D(Im.grid(), Im.memType())
    CreateRect(Imrect, c1, c2, val)
    Im += Imrect


def CreateRectReal(Im, corner1, corner2, val=1.0):
    '''Creates Image3D rectangle given two corners in world coordinates'''
    c1 = [min(corner1[0], corner2[0]), min(corner1[1], corner2[1])]
    c2 = [max(corner1[0], corner2[0]), max(corner1[1], corner2[1])]
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    x, y = np.ogrid[0:Im.size().x, 0:Im.size().y]
    origin = Im.grid().origin().tolist()
    spacing = Im.grid().spacing().tolist()
    x = origin[0] + spacing[0]*x
    y = origin[1] + spacing[1]*y

    # not 100% sure this is the right behavior
    Im.asnp()[:, :, :] = val*np.atleast_3d((x >= c1[0]) * (x <= c2[0]) *
                                           (y >= c1[1]) * (y <= c2[1]))
    Im.toType(oldType)

def AddRectReal(Im, c1, c2, val=1.0):
    '''Adds a rectangle to Im using CreateRectReal'''
    Imrect = ca.Image3D(Im.grid(), Im.memType())
    CreateRectReal(Imrect, c1, c2, val)
    Im += Imrect

def CreateCuboid(Im, corner1, corner2, val=1.0):
    '''Creates Image3D rectangle given two corners'''
    c1 = [min(corner1[0], corner2[0]), min(corner1[1], corner2[1]), min(corner1[2], corner2[2])]
    c2 = [max(corner1[0], corner2[0]), max(corner1[1], corner2[1]), max(corner1[2], corner2[2])]
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    x, y, z = np.ogrid[0:Im.size().x, 0:Im.size().y, 0:Im.size().z]

    # not 100% sure this is the right behavior
    Im.asnp()[:, :, :] = val*np.atleast_3d((x >= c1[0]) * (x < c2[0]) *
                                           (y >= c1[1]) * (y < c2[1]) *
                                           (z >= c1[2]) * (z < c2[2]))
    Im.toType(oldType)

def AddCuboid(Im, c1, c2, val=1.0):
    '''Adds a rectangle to Im using CreateRect'''
    Imcub = ca.Image3D(Im.grid(), Im.memType())
    CreateCuboid(Imcub, c1, c2, val)
    Im += Imcub

def CreateCuboidReal(Im, corner1, corner2, val=1.0):
    '''Creates Image3D rectangle given two corners in world coordinates'''
    c1 = [min(corner1[0], corner2[0]), min(corner1[1], corner2[1]), min(corner1[2], corner2[2])]
    c2 = [max(corner1[0], corner2[0]), max(corner1[1], corner2[1]), max(corner1[2], corner2[2])]
    ca.SetMem(Im, 0.0)
    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)
    x, y, z = np.ogrid[0:Im.size().x, 0:Im.size().y, 0:Im.size().z]
    origin = Im.grid().origin().tolist()
    spacing = Im.grid().spacing().tolist()
    x = origin[0] + spacing[0]*x
    y = origin[1] + spacing[1]*y
    z = origin[2] + spacing[2]*z

    Im.asnp()[:, :, :] = val*np.atleast_3d((x >= c1[0]) * (x <= c2[0]) *
                                           (y >= c1[1]) * (y <= c2[1]) *
                                           (z >= c1[2]) * (z <= c2[2]))
    Im.toType(oldType)

def AddCuboidReal(Im, c1, c2, val=1.0):
    '''Adds a rectangle to Im using CreateRect, using world instead of
    index coordinates'''
    Imcub = ca.Image3D(Im.grid(), Im.memType())
    CreateCuboid(Imcub, c1, c2, val)
    Im += Imcub
