'''functions to aid in creating HTML reports with Image3Ds, figures, etc'''
import HTMLGenFuncs
from PyCACalebExtras.Common import WritePNG, mkdir
from os.path import splitext

class htmlreport():
    '''object for creating an htmlreport in PyCA

    myhtmlreport = htmlreport('Results/mytest/myrun1')
    '''
    def __init__(self, prefix, title="Report"):
        self._body = ""
        self._prefix = prefix                 # e.g.: Results/folder/myrun
        self._savefigdir = prefix + '/'  # 'Results/folder/myrun/'
        self._htmlfigdir = './' + prefix.rpartition('/')[-1] + '/'  # './myrun/'
        self.title = title

        # list of names used for images
        self._imnames = []
        mkdir(self._prefix)


    def add_imlist(self, imlist, fname, scale=1.0, axis='default', rng=None,
                   dim='z', sliceIdx=None, size=None):
        for i, im in enumerate(imlist):
            self.add_image(im, fname + str(i).zfill(2), scale, axis, newline=False, size=size)
        self._body += "<br>"


    def add_image(self, im, fname, scale=1.0, axis='default', rng=None,
                  dim='z', sliceIdx=None, newline=True, size=None):
        assert fname not in self._imnames
        self._imnames += [fname, splitext(fname)[0]]
        if not fname.lower().endswith('.png'):
            fname += '.png'
        WritePNG(im, self._savefigdir + fname, axis, rng, dim, sliceIdx)
        # self._body += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Truerecon.png" />\n'
        if size is None and scale == 1.0:
            sizestr = ''
        elif scale != 1.0:
            print "PyCACalebExtras.HTML: scale not yet implemented!!!!!"
        else:
            sizestr = ' height="{0}" width="{1}" '.format(size[0], size[1])
            self._body += '<img' + sizestr + ' src="' + self._htmlfigdir + fname + '"/>\n'
        if newline:
            self._body += "<br>"



    def add_small_heading(self, text):
        self._body += '<h3>' + text + '</h3>\n'

    def add_med_heading(self, text):
        self._body += '<h2>' + text + '</h2>\n'

    def add_figure(self, figname, scale=1.0, fighandle=None, newline=True):
        assert figname not in self._imnames
        self._imnames += [figname, splitext(figname)[0]]

    def write(self):
        with open(self._prefix + '.html', 'w') as f:
            f.write(HTMLGenFuncs.GenHeadScript(self.title, defaultImScale=2.0) +
                    self._body +
                    HTMLGenFuncs.bodyHTMLEnd)
        print 'Wrote HTML Report ' + self._prefix
