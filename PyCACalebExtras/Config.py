# from collections import namedtuple
from copy import deepcopy
import yaml
import warnings


# config_dict = {
# 'Recon' : {'NumOS' : 20,
#            'NumIter' : 10},
# 'PoseEst' : {'TMaxPert' : 0},
# 'filename' : None
# }


class config_struct(object):
    '''a structure that makes up configs '''
    def __init__(self, **entries):
        self.__dict__.update(entries)


# def config():
#     '''returns a config dict initialized with defaults'''
#     return dict_to_struct(config_dict())


def dict_to_config(mydict):
    '''Takes a configuration dictionary object and returns a
    config object'''
    newdict = deepcopy(mydict)
    for key, value in mydict.iteritems():
        if isinstance(value, dict):
            newdict[key] = dict_to_config(value)
    return config_struct(**newdict)


def config_to_dict(mystruct):
    '''recursively converts a config into a dictionary'''
    newdict = {}
    for key in [a for a in dir(mystruct) if not a.startswith('__')]:
        if isinstance(getattr(mystruct, key), config_struct):
            newdict[key] = config_to_dict(getattr(mystruct, key))
        else:
            newdict[key] = getattr(mystruct, key)
    return newdict


def check_config(cfg, dref, key_prefix='', stacklevel=0):
    '''checks a config object 'cfg' to the reference dictionary 'dref'
    and prints a warning if there are any missing/superfluous keys'''

    if not isinstance(cfg, dict):
        cfg = config_to_dict(cfg)

    # print cfg
    # print dref

    cfgkeys = cfg.keys()
    drefkeys = dref.keys()

    for key in drefkeys:
        if key not in cfgkeys:
            warnings.warn("'{}' missing from config object".format(key_prefix+key),
                          stacklevel=stacklevel+2)
        elif isinstance(cfg[key], dict) and isinstance(dref[key], dict):
            check_config(cfg[key], dref[key], key_prefix+key+'.',
                         stacklevel=stacklevel+1)
        elif not isinstance(cfg[key], dict) and isinstance(dref[key], dict):
            printstr = "'{}' expected to be a config, not a value"
            warnings.warn(printstr.format(key_prefix+key),
                          stacklevel=stacklevel+2)
        elif isinstance(cfg[key], dict) and not isinstance(dref[key], dict):
            printstr = "'{}' should to be a value, not a config"
            warnings.warn(printstr.format(key_prefix+key),
                          stacklevel=stacklevel+2)

    for key in cfgkeys:
        if key not in drefkeys:
            printstr = "'{}' doesn't exist in reference config"
            warnings.warn(printstr.format(key_prefix+key),
                          stacklevel=stacklevel+2)
