import PyCA.Core as core
import PyCA.Common as common

import glob

import numpy as np

#
# All the text for the javascript / html
#

# takes the page title and original image height and width as
# parameters
htmlStartAndHeadFormat = """
<html>
<head>
<title>{title}</title>
<script type="text/javascript">
  var imSrcHash = new Object();
  var imDescHash = new Object();
  var imIdxHash = new Object();
  var imOrigSzHash = new Object();

  initScale = {initScale:n};


  function getImSizeAndResetOnLoad() {{
    var origSz = new Object();
    origSz.x = this.width;
    origSz.y = this.height;
    imOrigSzHash[this.obName] = origSz;
    resetIm(this.obName, 0);
    return true;
  }}

  function getImSizeAndReset(name) {{
    var myImage = new Image();
    // just get first image
    var imgSrc = imSrcHash[name][0];
    myImage.name = imgSrc;
    myImage.obName = name;
    myImage.onload = getImSizeAndResetOnLoad;
    myImage.src = imgSrc;
  }}

  function changeIm(name, imIdx) {{
    var imEl = document.getElementById(name);
    var imSrcList = imSrcHash[name];
    var imDescList = imDescHash[name];
    imEl.src = imSrcList[imIdx];
    var desc = imDescList[imIdx];
    imEl.title = desc;
    var infoCellEl = document.getElementById(name+'-info-div');
    infoCellEl.innerHTML = desc.replace(/\\n/g, '<br/>');

  }}

  function nextIm(name){{
    var imIdx = imIdxHash[name]+1;
    var imSrcList = imSrcHash[name];
    if(imIdx >= imSrcList.length) imIdx = 0;
    changeIm(name, imIdx)
    imIdxHash[name] = imIdx;
  }}

  function prevIm(name){{
    var imIdx = imIdxHash[name]-1;
    var imSrcList = imSrcHash[name];
    if(imIdx < 0) imIdx = imSrcList.length-1;
    changeIm(name, imIdx)
    imIdxHash[name] = imIdx;
  }}

  function zoomIn(name) {{
    var imEl = document.getElementById(name);
    setSize(name, imEl.width*2, imEl.height*2);
  }}

  function zoomOut(name) {{
    var imEl = document.getElementById(name);
    setSize(name, imEl.width/2, imEl.height/2);
  }}

  function setZoom(name, fac) {{
    var imEl = document.getElementById(name);
    var origSz = imOrigSzHash[name];
    setSize(name, origSz.x*fac, origSz.y*fac);
  }}

  function setSize(name, width, height) {{
    var imEl = document.getElementById(name);
    imEl.width=width;
    imEl.height=height;
    var tableEl = document.getElementById(name+'-info-div');
    var widthStr = width.toString()+'px'
    console.log(widthStr)
    tableEl.style.width = widthStr
  }}

  function initAll(){{
    for (var ob in imSrcHash) {{
      //console.log(ob);
      getImSizeAndReset(ob)
    }}
  }}

  function resetIm(name) {{
    changeIm(name, 0)
    setZoom(name, initScale)
  }}

  function toggleVisible(name) {{
    var el = document.getElementById(name);
    if(el.style.visibility == 'visible'){{
      el.style.visibility = 'collapse'
    }}else{{
      el.style.visibility = 'visible'
    }}
  }}

</script>
</head>
<body onload="initAll()">
"""
usageHTML = """
<h3>Usage</h3>
The following buttons are available for images / image series:
<dl>
<dt>[<],[>]</dt>
<dd>proceed to the previous / next image in the series.
Clicking the image has the same effect as the [>] button.</dd>
<dt>[-],[+]</dt>
<dd>Zoom in or out on the specified image (factor of 0.5/2.0)</dd>
<dt>[r]</dt>
<dd>reset to original settings</dd>
<dt>[i]</dt>
<dd>toggle display of image details</dd>
"""

# Format string:
# obID - string object ID
# srcArrayString - string consisting of quoted image locations separated by commmas
# descArrayString - string consisting of quote descriptions separated by commas
imageScriptFormat = """
<script type="text/javascript">
  imSrcHash['{obID}']=
    Array({srcArrString});
  imDescHash['{obID}']=
    Array({descArrString});
  imIdxHash['{obID}']=0;
</script>
"""

# Format taking unquoted object name
imageHTMLFormat = """
<table border=1 style="table-layout:fixed;">
<tr>
<td colspan=6 align="center">
<img height=128 width=128 src="" id="{obID}" alt="orig range:"
     onmousedown="nextIm('{obID}')"
     title="">
</td>
</tr>
<tr>
<td align="center"><button onclick="prevIm('{obID}')">[<]</button></td>
<td aligh="center"><button onclick="zoomOut('{obID}')">[-]</button></td>
<td aligh="center"><button onclick="resetIm('{obID}')">[r]</button></td>
<td aligh="center"><button onclick="toggleVisible('{obID}-info-row')">[i]</button></td>
<td align="center"><button onclick="zoomIn('{obID}')">[+]</button></td>
<td align="center"><button onclick="nextIm('{obID}')">[>]</button></td>
</tr>
<tr id="{obID}-info-row" style="visibility:visible">
<td colspan=6 align="center" id="{obID}-info-cell">
<div style="position:relative;width:128px" id="{obID}-info-div">
info will go here
</div>
</td>
</tr>
</table>
"""

bodyHTMLEnd = """
</body>
</html>
"""

# global object counter
global obCount
obCount = 0


def GenHeadScript(title='page title', defaultImScale=0.25):
    return htmlStartAndHeadFormat.format(title=title,
                                         initScale=defaultImScale)


def GenUsage():
    return usageHTML


def GenImageSeriesHTML(imFileList, imInfoList=None, obID=None):
    global obCount

    if obID is None:
        obID = 'object%03d' % obCount
        obCount += 1
    if imInfoList is None:
        imInfoList = imFileList

    imhtml = ""
    fileArrString = reduce(lambda x, y: x + ',' + y,
                           map(lambda x: '"' + x + '"', imFileList))
    descArrString = reduce(lambda x, y: x + ',' + y,
                           map(lambda x: '"' + x + '"', imInfoList))
    imhtml += imageScriptFormat.format(obID=obID,
                                       srcArrString=fileArrString,
                                       descArrString=descArrString)
    imhtml += imageHTMLFormat.format(obID=obID)

    print imFileList
    return imhtml


def GenPNGSeriesHTML(prefix, fname):
    global obCount
    imFileList = sorted(glob.glob(prefix + '/figs/' + fname + '*.png'))
    cutoff = len(prefix) + 1
    for i in range(len(imFileList)):
        imFileList[i] = imFileList[i][cutoff:]
    print prefix
    print fname
    print imFileList

    if len(imFileList) > 0:

        obID = 'object%03d' % obCount
        obCount += 1
        imhtml = ""
        fileArrString = \
            reduce(lambda x, y: x + ',' + y,
                   map(lambda x: '"'+x+'"', imFileList))
        imhtml += imageScriptFormat.format(obID=obID,
                                           srcArrString=fileArrString,
                                           descArrString=fileArrString)
        imhtml += imageHTMLFormat.format(obID=obID)
        return imhtml
    else:
        print "Unable to find previously generated PNGs"
        return ''


#
# functions for generating series from numpy or core.Image3D objects
#
def GenSliceImageSeriesHTML(imSliceList, imNameList,
                            indepScale=True, imDescList=None,
                            baseDir='.', figDir='.',
                            t=False, cmap='gray'):
    import matplotlib.pyplot as plt

    imSrcList = []
    imTextList = []
    nIm = len(imSliceList)

    if type(indepScale) == bool:
        indepScale = [indepScale]*nIm

    imRangeList = map(lambda im: core.MinMax(im), imSliceList)
    imRangeList = zip(*imRangeList)
    imRangeList.append(indepScale)
    imRangeList = zip(*imRangeList)
    # filter based on indepScale value
    coScale = filter(lambda x: (not x[2]), imRangeList)
    # range across images with indepScale == False
    if len(coScale) > 0:
        minMax = reduce(lambda x, y: [min(x[0], y[0]),
                                      max(x[1], y[1])], coScale)
    # rescale and write images
    for imIdx in range(nIm):
        im = imSliceList[imIdx]
        imInfo = imRangeList[imIdx]
        if imInfo[2]:
            imWin = imInfo[:2]
        else:
            imWin = minMax
        # rescale
        rescaled = core.Image3D(im.grid(), im.memType())
        if imWin[1]-imWin[0] > 0:
            core.SubC(rescaled, im, imWin[0])
            core.DivC_I(rescaled, imWin[1]-imWin[0])
            # truncate
            core.MinC_I(rescaled, 1.0)
            core.MaxC_I(rescaled, 0.0)
        else:
            core.Copy(rescaled, im)
        rescaled.toType(core.MEM_HOST)
        imArr = rescaled.asnp()
        # transpose
        if t:
            # does not actually copy object, just new view
            imArr = imArr.T
        # write
        fname = figDir + "/" + imNameList[imIdx] + ".png"
        plt.imsave(baseDir + "/" + fname,
                   np.squeeze(imArr), cmap=cmap,
                   vmin=0.0, vmax=1.0)
        imSrcList.append(fname)
        if imDescList is not None:
            desc = imDescList[imIdx] + "\\n"
        else:
            desc = ""
            desc += fname + "\\n" + \
                "Range: %f/%f\\n" % (imInfo[0], imInfo[1]) + \
                "Win: %f/%f\\n" % (imWin[0], imWin[1])
        imTextList.append(desc)

    imhtml = GenImageSeriesHTML(imSrcList, imTextList)
    return imhtml


def GenImage3DSeriesHTML(imList, imNameList,
                         indepScale=True, imDescList=None,
                         sliceIdx=None, dim='z',
                         baseDir='.', figDir='.',
                         t=False, cmap='gray'):
    imSliceList = []
    for im in imList:
        sliceIm = common.ExtractSliceIm(im, sliceIdx=sliceIdx, dim=dim)
        imSliceList.append(sliceIm)
    imhtml = GenSliceImageSeriesHTML(imSliceList, imNameList,
                                     indepScale=indepScale,
                                     imDescList=imDescList,
                                     baseDir=baseDir, figDir=figDir,
                                     t=t, cmap=cmap)
    return imhtml


#
# Test function, just show generation of page from one series
#
def GenHTML(imList, nameList,
            indepScale=True,
            imDescList=None,
            baseDir='.', figDir='.',
            htmlname='index.html', title='page title',
            imSz=[256, 256],
            cmap='gray', t=False):
    page = GenHeadScript(title, imSz)
    page += GenImage3DSeriesHTML(imList, nameList,
                                 indepScale=indepScale,
                                 imDescList=imDescList,
                                 baseDir=baseDir, figDir=figDir,
                                 t=t, cmap=cmap)
    newNames = map(lambda name: name+'-jet', nameList)
    page += GenImage3DSeriesHTML(imList, newNames,
                                 indepScale=indepScale,
                                 imDescList=imDescList,
                                 baseDir=baseDir, figDir=figDir,
                                 t=t, cmap='jet')
    page += bodyHTMLEnd

    f = open(baseDir+'/'+htmlname, 'w')
    f.write(page)
    f.close()
