'''PyCACalebExtras.Display - display functions for displaying PyCA
Image3Ds and Field3Ds

For (nearly) all of these 2D display options, we have two axis
options: 'default' and 'cart'. 'default' displays images in the 'PNG'
coordinate system (x=array row, y=array column):

------>           ------->              -------->
|    y            |     z               |      z
|                 |                     |
|x                |x                    |y
v                 v                     v

The 'cart' display option displays images in the cartesian coordinate system:

^              ^              ^
|y             |z             |z
|              |              |
|      x       |      x       |      y
|-------->     |-------->     |-------->

'''
import sys
import scipy.interpolate as interp
import time
import numpy as np

import PyCA.Core as ca
import PyCA.Common as common
from PyCACalebExtras.Common import Is3D, JacDetH, JacDetV, OffDim, GetAspect, \
    MinMaxList, ResampleWorld, MakeGrid, ReorderAxes, BlendObjs, SetRange_I


class PyCACalebExtrasDisplayException(Exception):
    '''exception for this class'''
    pass


def DispImage(im, rng=None, cmap='gray', title=None,
              newFig=True, colorbar=False, axis='default',
              dim='z', sliceIdx=None):
    ''' Displays a PyCA Image3D - defaults to a normal grayscale image

    rng is the intensity range (defaults to data intensity limits)
    cmap is the colormap ('gray', 'jet', etc)
    axis is the axis direction.  'default' has (0,0) in the upper left hand corner
                                      and the x direction is vertical
                                 'cart' has (0,0) in the lower left hand corner
                                      and the x direction is horizontal
    If im is a 3D volume, a central slice along dimension dim={'x','y','z'} is viewed
    '''
    import matplotlib.pyplot as plt
    plt.ion()  # tell it to use interactive mode -- see results immediately

    if Is3D(im):
        if isinstance(im, ca.Image3D):
            im = common.ExtractSliceIm(im, dim=dim, sliceIdx=sliceIdx)
        else:
            im = common.ExtractSliceVF(im, dim=dim, sliceIdx=sliceIdx)
    else:
        dim = OffDim(im)
    # create the figure if requested
    if newFig:
        img = plt.figure()
    plt.clf()               # don't be slow, also clear colorbars if necessary

    if rng is None:
        mm = ca.MinMax(im)
        vmin = mm[0]
        vmax = mm[1]
        if mm[1] - mm[0] == 0:
            vmin -= 1
            vmax += 1
    else:
        vmin, vmax = rng        # pylint: disable=W0633

    oldType = im.memType()
    im.toType(ca.MEM_HOST)

    if isinstance(im, ca.Field3D):  # for color images
        # color images need to be in 0-1 range
        if ca.Max(im) > 1.0:
            denom = max(2.0**(np.ceil(np.log2(ca.Max(im)))), 1.0)  # nextpow2
        else:
            denom = 1.0
        imnp = np.concatenate((im.asnp()[0].copy()/denom, im.asnp()[1].copy()/denom,
                               im.asnp()[2].copy()/denom), 2)
    else:
        imnp = np.squeeze(im.asnp().copy())

    if np.isnan(imnp).any():
        raise PyCACalebExtrasDisplayException("DispImage: Image contains NANs, cannot plot")

    aspect = GetAspect(im.grid(), dim, axis, True)

    if axis == 'default':
        img = plt.imshow(imnp,
                         cmap=cmap, vmin=vmin, vmax=vmax, aspect=aspect)

    else:                       # axis == 'cart'
        if isinstance(im, ca.Field3D):  # for color images
            img = plt.imshow(np.squeeze(imnp.transpose(1, 0, 2)),
                             cmap=cmap, vmin=vmin, vmax=vmax, aspect=aspect)
        else:
            img = plt.imshow(np.squeeze(imnp.transpose()),
                             cmap=cmap, vmin=vmin, vmax=vmax, aspect=aspect)
        plt.gca().invert_yaxis()

    im.toType(oldType)

    plt.xticks([])              # no ticks
    plt.yticks([])
    plt.axis('off')             # no border
    if title is not None:
        plt.title(title)
    img.set_interpolation('nearest')

    if colorbar:
        plt.colorbar()
    plt.draw()
    plt.show()                  # plt.ion() doesn't always work correctly
    plt.pause(0.0000001)        # this is bullshit

    return img


def DispImage3D(im, rng=None,
                cmap='gray', title=None,
                newFig=True, colorbar=False, dim='z', axis='default'):
    '''Displays a "movie" through a 3D block of an Image3D (going from
    dim=0 to dim=end)
    '''
    import matplotlib.pyplot as plt
    plt.ion()  # tell it to use interactive mode -- see results immediately

    # create the figure if requested
    if newFig:
        plt.figure()

    if rng is None:
        mm = ca.MinMax(im)
        if mm[1] - mm[0] == 0:
            vmin = mm[0]-1
            vmax = mm[0]+1
        else:
            vmin, vmax = mm
        rng = [vmin, vmax]

    if dim is 'z':
        imlength = im.grid().size().z
    elif dim is 'y':
        imlength = im.grid().size().y
    else:
        imlength = im.grid().size().x

    for i in xrange(imlength):
        if isinstance(im, ca.Image3D):
            imslc = common.ExtractSliceIm(im, i, dim)
        else:
            imslc = common.ExtractSliceVF(im, i, dim)
        DispImage(imslc, rng, cmap, title, False, colorbar, axis)
        time.sleep(.01)


def DispVMag(v, rng=None,
             cmap='jet', title=None,
             newFig=True, colorbar=True):
    '''Displays the magnitude of a vector Field3D v'''

    if not isinstance(v, ca.Field3D):
        raise Exception('PyCACalebExtras.Display: Input not an PyCA Field3D')

    vmag = ca.Image3D(v.grid(), v.memType())
    ca.SqrMagnitude(vmag, v)
    ca.Sqrt_I(vmag)
    return DispImage(vmag, rng, cmap, title, newFig, colorbar)


def DispHMag(h, rng=None,
             cmap='jet', title=None,
             newFig=True, colorbar=True):
    '''Displays the magnitude of a displacement Field3D h'''

    v = ca.Field3D(h.grid(), h.memType())
    ca.HtoV(v, h)
    return DispVMag(v, rng, cmap, title, newFig, colorbar)


def DispVGrid(v, grid_size=None, title=None,
              newFig=True, splat=False, fig_len=7, dim='z', axis=None,
              sliceIdx=None):
    '''Displays a grid of the vector field v'''

    h = ca.Field3D(v.grid(), v.memType())
    ca.VtoH(h, v)
    DispHGrid(h, grid_size, title, newFig, splat, fig_len, dim, axis,
              sliceIdx)


def DispHGrid(h, grid_size=None, title=None,
              newFig=True, splat=False, fig_len=7, dim='z', axis=None,
              sliceIdx=None):
    '''Displays a grid of the displacement field h'''
    import matplotlib.pyplot as plt
    plt.ion()  # tell it to use interactive mode -- see results immediately

    if not isinstance(h, ca.Field3D):
        raise Exception('PyCACalebExtras.Display: Input not an PyCA Field3D')

    if Is3D(h):
        h = common.ExtractSliceVF(h, dim=dim, sliceIdx=sliceIdx)

    mType = h.memType()
    h.toType(ca.MEM_HOST)
    if dim == 'z':
        hx = np.squeeze(h.asnp())[0, :, :]
        hy = np.squeeze(h.asnp())[1, :, :]
        sx = h.grid().size().x
        sy = h.grid().size().y
        spx = h.grid().spacing().x
        spy = h.grid().spacing().y
    elif dim == 'y':        # not tested
        hx = np.squeeze(h.asnp())[0, :, :]
        hy = np.squeeze(h.asnp())[2, :, :]
        sx = h.grid().size().x
        sy = h.grid().size().z
        spx = h.grid().spacing().x
        spy = h.grid().spacing().z
    elif dim == 'x':        # not tested
        hx = np.squeeze(h.asnp())[1, :, :]
        hy = np.squeeze(h.asnp())[2, :, :]
        sx = h.grid().size().y
        sy = h.grid().size().z
        spx = h.grid().spacing().y
        spy = h.grid().spacing().z
    realsx = (sx-1)*spx
    realsy = (sy-1)*spy

    if splat:
        y, x = np.meshgrid(np.arange(sy), np.arange(sx))
        # This is the slow step
        hxnew = interp.griddata((hy.ravel(), hx.ravel()),
                                x.ravel(), (y, x))
        hynew = interp.griddata((hy.ravel(), hx.ravel()),
                                y.ravel(), (y, x))
        hx = hxnew              # get an error if i don't do this
        hy = hynew

    # account for image spacing
    hx *= spx
    hy *= spy

    if axis == 'cart':
        hx, hy = hy.T, hx.T
    elif axis is not None:
        raise Exception("PyCACalebExtras.Display: unknown value for 'axis'")

    # create the figure if requested
    # TODO: Non square grid plots
    if newFig:
        grid = plt.figure(figsize=(fig_len, fig_len), dpi=140)
        grid.set_facecolor('white')
    else:
        plt.clf()
    if title is not None:
        plt.title(title)

    # dont allow for more than 127 lines
    if grid_size is None:
        grid_sizex = max(sx/64, 1)
        grid_sizey = max(sy/64, 1)
    else:
        grid_sizex = grid_size
        grid_sizey = grid_size

    hx_sample_h = hx[grid_sizex/2::grid_sizex, :]
    hy_sample_h = hy[grid_sizex/2::grid_sizex, :]

    hx_sample_v = hx[:, grid_sizey/2::grid_sizey]
    hy_sample_v = hy[:, grid_sizey/2::grid_sizey]

    # TODO:actually cut off outside when splatting??
    # plotting
    plt.hold(True)
    # plot horizontal lines (y values)
    plt.plot(hy_sample_h.transpose(), hx_sample_h.transpose(), 'k')
    # plot vertical lines (x values)
    plt.plot(hy_sample_v, hx_sample_v, 'k')
    # make grid look nicer
    plt.axis('off')

    if splat:
        pad = 0.03              # shrink fig when splatting
    else:
        pad = -0.01

    # keep the figure square, but make sure the whole grid fits in the fig
    maxax = max(realsy, realsx)
    minax = pad*maxax
    maxax = (1-pad)*maxax

    if axis == 'cart':
        plt.axis([minax, maxax, minax, maxax])
    else:                       # flip y axis
        plt.axis([minax, maxax, maxax, minax])
    h.toType(mType)
    plt.draw()
    plt.show()
    plt.pause(0.0000001)

def DispJacDetH(h, rng=None,
                cmap='jet', title=None,
                newFig=True, colorbar=True):
    '''displays the estimated jacobian determinant'''
    if not isinstance(h, ca.Field3D):
        raise Exception('PyCACalebExtras.Display: Input not an PyCA Field3D')

    jacdet = ca.Image3D(h.grid(), h.memType())
    # scratchI = ca.Image3D(h.grid(), h.memType())
    JacDetH(jacdet, h)
    # g = ca.GaussianFilterGPU()
    # g.updateParams(h.size(), ca.Vec3Df(1.0, 1.0, 1.0), ca.Vec3Di(9))
    # g.filter(jacdet, jacdet, scratchI)
    DispImage(jacdet, rng, cmap, title, newFig, colorbar)


def DispJacDetV(h, rng=None,
                cmap='jet', title=None,
                newFig=True, colorbar=True):
    '''displays the estimated (smoothed) jacobian determinant of a
    displacement Field3D h'''

    if not isinstance(h, ca.Field3D):
        raise Exception('PyCACalebExtras.Display: Input not an PyCA Field3D')

    jacdet = ca.Image3D(h.grid(), h.memType())
    # scratchI = ca.Image3D(h.grid(), h.memType())
    JacDetV(jacdet, h)
    # g = ca.GaussianFilterGPU()
    # g.updateParams(h.size(), ca.Vec3Df(1.0, 1.0, 1.0), ca.Vec3Di(9))
    # g.filter(jacdet, jacdet, scratchI)
    DispImage(jacdet, rng, cmap, title, newFig, colorbar)


def Disp3Pane(im, rng=None, sz=None, sliceIdx=None, disp=True, colorbar=False,
              cmap='gray', title=None, bgval=0.0, CS='LPS'):
    '''returns a 3 pane *3D. Defaults to 3 sz^2 panes (and one blank one)
    where sz is the 2nd largest size found in the image (but sz can be specified)

    The sliceIdx is a list of 3 indices (None indicates middle slice):
    [sliceIdx (axial, upper left), sliceIdx (coronal, lower left),
        sliceIdx (saggital, lower right)]
    For this, a higher number is going INTO the screen toward the Superior,
        Posterior, and Left (same indices as VolView)

    returns the final Image3D (in case you want to save it)
    if disp=True, it will display it just like DispImage does

    bgval is the value (float) for the background: both for the lower right corner
    and for the other backgrounds (for non square images)

    CS is the coordinate system, e.g., RAS, LAS, RPS.

    the three pane displayed axes are:


    for LPS coord sys:            For all coord sys (same as volview)
    ________________> y           ________________> y
    |  ____> x                    | ^ A  (axl)
    | |                           | |
    | |                           | |____> L
    | v y                         |
    |                             |
    | ^ z       ^ z               | ^ S       ^ S
    | |         |                 | |  (cor)  |        (sag)
    | |____> x  |____> y          | |____> L  |____> P
    |                             |
    v x                           v x

    '''
    # original size and spacing
    spx, spy, spz = im.spacing().tolist()
    szx, szy, szz = im.size().tolist()

    if sliceIdx is None:
        sliceIdx = [None, None, None]
    if sz is None:
        sz = sorted([szx, szy, szz])[1]
        smallsz = ca.Vec3Di(sz, sz, 1)
    else:
        smallsz = ca.Vec3Di(sz, sz, 1)  # currently only support square 3-panes
    bigsz = ca.Vec3Di(2*sz, 2*sz, 1)

    numtodim = {0: 'x', 1: 'y', 2: 'z'}
    dimtonum = {'x': 0, 'y': 1, 'z': 2}
    axl_dim = numtodim[max(CS.find('S'), CS.find('I'))]
    cor_dim = numtodim[max(CS.find('P'), CS.find('A'))]
    sag_dim = numtodim[max(CS.find('L'), CS.find('R'))]
    if len(np.unique([axl_dim, cor_dim, sag_dim])) != 3:
        raise Exception('Bad Coordinate system!')

    # Find proper location for sliceIdx
    # here, the indices of sliceIdx don't refer to z/y/x, they refer to axl/cor/sag
    [axl_sliceIdx, cor_sliceIdx, sag_sliceIdx] = sliceIdx  # pylint: disable=W0633
    sz = [szx, szy, szz]
    if axl_sliceIdx is not None and CS[dimtonum[axl_dim]] == 'I':  # look towards Superior
        axl_sliceIdx = sz[dimtonum[axl_dim]] - axl_sliceIdx - 1
    if cor_sliceIdx is not None and CS[dimtonum[cor_dim]] == 'A':  # look towards Posterior
        cor_sliceIdx = sz[dimtonum[cor_dim]] - cor_sliceIdx - 1
    if sag_sliceIdx is not None and CS[dimtonum[sag_dim]] == 'R':  # look towards Left
        sag_sliceIdx = sz[dimtonum[sag_dim]] - sag_sliceIdx - 1
        # Note: oddly enough, this creates an improper coordinate change,
        # but VolView does it, so i'll follow their convention

    if isinstance(im, ca.Image3D):
        im_axl = common.ExtractSliceIm(im, dim=axl_dim, sliceIdx=axl_sliceIdx)
        im_cor = common.ExtractSliceIm(im, dim=cor_dim, sliceIdx=cor_sliceIdx)
        im_sag = common.ExtractSliceIm(im, dim=sag_dim, sliceIdx=sag_sliceIdx)
        im_axl_sq, im_cor_sq, im_sag_sq = [ca.Image3D(ca.GridInfo(smallsz), im.memType())
                                           for _ in xrange(3)]
        imbig = ca.Image3D(ca.GridInfo(bigsz), im.memType())

    else:
        im_axl = common.ExtractSliceVF(im, dim=axl_dim, sliceIdx=axl_sliceIdx)
        im_cor = common.ExtractSliceVF(im, dim=cor_dim, sliceIdx=cor_sliceIdx)
        im_sag = common.ExtractSliceVF(im, dim=sag_dim, sliceIdx=sag_sliceIdx)
        im_axl_sq, im_cor_sq, im_sag_sq = [ca.Field3D(ca.GridInfo(smallsz), im.memType())
                                           for _ in xrange(3)]
        imbig = ca.Field3D(ca.GridInfo(bigsz), im.memType())

    CS = CS.upper()

    # relate CS to the 2D axes the display for the 3 slicing planes
    axl_xform = {'R': '-y', 'L': 'y',
                 'P': 'x', 'A': '-x',
                 'S': 'z', 'I': '-z'}  # doesn't matter
    cor_xform = {'R': '-y', 'L': 'y',
                 'P': 'z', 'A': '-z',  # doesn't matter
                 'S': '-x', 'I': 'x'}
    sag_xform = {'R': 'z', 'L': '-z',  # doesn't matter
                 'P': 'y', 'A': '-y',
                 'S': '-x', 'I': 'x'}

    im_axl = ReorderAxes(im_axl, [axl_xform[d] for d in CS])
    im_cor = ReorderAxes(im_cor, [cor_xform[d] for d in CS])
    im_sag = ReorderAxes(im_sag, [sag_xform[d] for d in CS])

    # set origin to center of the block
    for im in [im_axl, im_cor, im_sag]:
        im.setGrid(MakeGrid(im.size(), im.spacing(), 'center'))

    # set appropriate spacing for panes
    maxlen = max(spx*szx, spy*szy, spz*szz)
    sqsp = 1.0*maxlen/smallsz.x
    for im in [im_axl_sq, im_cor_sq, im_sag_sq]:
        im.setGrid(MakeGrid(im.size(), [sqsp, sqsp, 1.0], 'center'))

    # subtract the background value
    im_axl -= bgval
    im_cor -= bgval
    im_sag -= bgval

    # resample onto panes
    bg = ca.BACKGROUND_STRATEGY_PARTIAL_ZERO
    ResampleWorld(im_axl_sq, im_axl, bg)
    ResampleWorld(im_cor_sq, im_cor, bg)
    ResampleWorld(im_sag_sq, im_sag, bg)

    ca.SetMem(imbig, 0.0)

    imbig.setGrid(MakeGrid(bigsz, [sqsp, sqsp, 1.0], 'center'))
    ca.SetSubVol_I(imbig, im_axl_sq, ca.Vec3Di(0, 0, 0))
    ca.SetSubVol_I(imbig, im_cor_sq, ca.Vec3Di(smallsz.x, 0, 0))
    ca.SetSubVol_I(imbig, im_sag_sq, ca.Vec3Di(smallsz.x, smallsz.y, 0))

    imbig += bgval

    if disp:                    # otherwise, just return the image
        # DispImage(im_sag_sq, axis='cart')
        DispImage(imbig, rng=rng, cmap=cmap, title=title, colorbar=colorbar)

    return imbig


def DispImlist(l, rng=None, cmap='gray', title=None,
               colorbar=False, axis='default',
               dim='z', sliceIdx=None):

    '''works like DispImage, but takes a list of images, and you can
    scroll through these'''
    import matplotlib.pyplot as plt
    plt.ion()  # tell it to use interactive mode -- see results immediately

    # TODO: use axis option
    # TODO - get it to work with color images

    class IndexTracker(object):
        '''keeps track of current image being displayed'''
        def __init__(self, ax, X, aspect):
            self.ax = ax
            if title is None:
                ax.set_title('use scroll wheel to navigate images')
            else:
                ax.set_title(title)
            self.X = X
            self.slices = X.shape[2]
            self.ind = self.slices/2

            if rng is None:
                vmin, vmax = np.min(X), np.max(X)
                if vmin == vmax:
                    vmin -= 1
                    vmax += 1
            else:
                vmin, vmax = rng  # pylint: disable=W0633

            self.im = ax.imshow(np.squeeze(self.X[:, :, self.ind, :]),
                                cmap=cmap, vmin=vmin, vmax=vmax,
                                aspect=aspect)
            self.time = time.time()
            plt.xticks([])
            plt.yticks([])
            self.im.set_interpolation('nearest')
            plt.axis('off')
            if colorbar:
                plt.colorbar()
            self.update()

        def onscroll(self, event):
            '''dictates what happens on a scroll event'''
            # update self.ind, call update()
            if event.button == 'up':
                self.ind = np.clip(self.ind+1, 0, self.slices-1)
            else:
                self.ind = np.clip(self.ind-1, 0, self.slices-1)
            self.update()

        def update(self):
            '''update the current slice'''
            self.im.set_data(np.squeeze(self.X[:, :, self.ind, :]))
            ax.set_ylabel('slice %s' % self.ind)
            self.im.axes.figure.canvas.draw()

        def __del__(self):
            if abs(time.time() - self.time) < .3:
                print 'Warning: scroll object deleted. ' + \
                    'Make sure to return (dummy) object from DispImList'
            self.ax.set_title('[Scroll object deleted]')

    fig = plt.figure()
    ax = fig.add_subplot(111)

    if isinstance(l[0], ca.Field3D):
        X = np.zeros((l[0].grid().size().x, l[0].grid().size().y, len(l), 3))
    else:
        X = np.zeros((l[0].grid().size().x, l[0].grid().size().y, len(l), 1))

    for i, ob in enumerate(l):
        if Is3D(ob):
            if isinstance(ob, ca.Image3D):
                ob = common.ExtractSliceIm(ob, dim=dim, sliceIdx=sliceIdx)
            else:
                ob = common.ExtractSliceVF(ob, dim=dim, sliceIdx=sliceIdx)
        else:
            dim = OffDim(ob)
        oldType = ob.memType()
        ob.toType(ca.MEM_HOST)
        if isinstance(ob, ca.Field3D):
            for j in xrange(3):
                # print X[:, :, i, j].shape
                # print ob.asnp()[j][:,:
                X[:, :, i, j] = np.squeeze(ob.asnp()[j][:, :])

        else:
            X[:, :, i, :] = ob.asnp()
        ob.toType(oldType)

    # if color, make sure its in the 0-1 range
    if isinstance(ob, ca.Field3D):
        if np.max(X) > 1.0:
            denom = max(2.0**(np.ceil(np.log2(np.max(X)))), 1.0)  # nextpow2
        else:
            denom = 1.0
        X /= denom

    aspect = GetAspect(l[0].grid(), dim, axis, True)
    tracker = IndexTracker(ax, X, aspect)
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    plt.show()
    return tracker              # must assign!!


def DispBlend(ob1, ob2, numblends=10, rng=None, cmap='gray', title=None,
              colorbar=False, axis='default', dim='z', sliceIdx=None):
    '''blends between two images'''
    oblist = []
    if isinstance(ob1, ca.Image3D):
        ob1 = common.ExtractSliceIm(ob1, dim=dim, sliceIdx=sliceIdx)
    else:
        ob1 = common.ExtractSliceVF(ob1, dim=dim, sliceIdx=sliceIdx)
    if isinstance(ob2, ca.Image3D):
        ob2 = common.ExtractSliceIm(ob2, dim=dim, sliceIdx=sliceIdx)
    else:
        ob2 = common.ExtractSliceVF(ob2, dim=dim, sliceIdx=sliceIdx)
    SetRange_I(ob1)
    SetRange_I(ob2)

    for a in np.linspace(0, 1, numblends):
        obblend = BlendObjs(ob1, ob2, a)
        oblist.append(obblend)

    return DispImlist(oblist, rng=rng, cmap=cmap, title=title, colorbar=colorbar,
                      axis=axis, dim=dim, sliceIdx=None)


def Disp3PaneBlend(im1, im2, numblends=10, rng=None, sz=None, sliceIdx=None,
                   colorbar=False, cmap='gray', title=None, bgval=0.0, CS='LPS'):
    ''' blends 3panes between two images'''

    im1_3p = Disp3Pane(im1, rng, sz, sliceIdx, disp=False, bgval=bgval, CS=CS)
    im2_3p = Disp3Pane(im2, rng, sz, sliceIdx, disp=False, bgval=bgval, CS=CS)

    return DispBlend(im1_3p, im2_3p, numblends, rng=rng, cmap=cmap, title=title,
                     colorbar=colorbar, dim='z', sliceIdx=None)


def Histogram(Im, bins=10, rng=None, normed=False,
              weights=None, density=None):
    '''Displays a numpy histogram of an Image3D '''
    import matplotlib.pyplot as plt
    plt.ion()  # tell it to use interactive mode -- see results immediately

    oldType = Im.memType()
    Im.toType(ca.MEM_HOST)

    hist = np.histogram(Im.asnp().copy(), bins, rng,
                        normed, weights, density)
    # xcent = []
    width = hist[1][1]-hist[1][0]
    # for i in xrange(1, bins+1):
    #     xcent.append(hist[1][i-1] + width/2)
    xcent = hist[1][:-1]

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.bar(xcent, hist[0], width)

    Im.toType(oldType)


def HistogramLine(Imlist, bins=100, rng=None, legend=None, norm=False):
    '''Histogram Line Charts for one or multiple images'''
    import matplotlib.pyplot as plt
    plt.ion()  # tell it to use interactive mode -- see results immediately

    try:                        # in case Imlist is just a single image
        Imlist[0]
    except TypeError:
        Imlist = [Imlist]


    if rng is None:
        rng = MinMaxList(Imlist)
    oldType = [Im.memType() for Im in Imlist]
    for Im in Imlist:
        Im.toType(ca.MEM_HOST)

    # fig = plt.figure()
    plt.figure()
    histograms = []
    for Im in Imlist:
        cnt, x = np.histogram(Im.asnp().copy(), bins, rng, norm)
        histograms.append(cnt)

    histograms = np.array(histograms).T/bins
    x = x[:-1] - .5*(x[1]-x[0])

    plt.plot(x, histograms)
    if legend is not None:
        plt.legend(legend)

    plt.xlim(x[0], x[-1])

    for Im, mtype in zip(Imlist, oldType):
        Im.toType(mtype)


def EnergyPlot(energy, title='Energy', newFig=True, legend=None):
    '''Plots energies

    The energies should be in the form
    [E1list, E2list, E3list, ...]
    with the legend as
    [E1legend, E2legend, E3legend]
    '''
    import matplotlib.pyplot as plt
    plt.ion()  # tell it to use interactive mode -- see results immediately
    # energy should be a list of lists, or just a single list
    if newFig:
        plt.figure()
    plt.clf()
    en = np.array(energy)
    plt.plot(en.T)
    if legend is not None:
        plt.legend(legend)
    if title is not None:
        plt.title(title)
    plt.draw()
    plt.show()
    plt.pause(0.0000001)
